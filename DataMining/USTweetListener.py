import tweepy
import keys as keys
from sys import exit
 
# Setting up the keys
# NOTE: Assumes that we have a file "keys.py" stored in our working directory. "keys.py" contains our Twitter account keys and secret keys to enable the API calls. 
consumer_key = keys.consumer_key
consumer_secret = keys.consumer_secret
access_token = keys.access_token
access_secret = keys.access_secret
 
#printing all the tweets to the standard output
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_secret)

upper_limit = 220000
count = 0
 
api = tweepy.API(auth)
places = api.geo_search(query="USA", granularity="country")
place_id = places[0].id
# print "place_id: " + str(place_id) # 96683cc9126741d1
print "TIME, TWEET_TEXT, PLACE_NAME, LATITUDE, LONGITUDE" 

for tweet in tweepy.Cursor(api.search,
                           q="place:%s" % place_id,
                           count=100,
                           # until = '2014-01-01',
                           # result_type="recent",
                           include_entities=True,
                           lang="en").items():
    latitude = 'Undefined latitude'
    longitude = 'Undefined longitude'
    if ((tweet.geo is not None) and ('coordinates' in tweet.geo)): # error for "NoneType has no attr 'coordinates'"
        latitude = tweet.geo['coordinates'][0]
        longitude = tweet.geo['coordinates'][1]
        
    print str(tweet.created_at) + ', ' + tweet.text.replace(',','').replace('\n','').replace('\r\n','').encode('ascii','ignore') + ', ' + (tweet.place.name if tweet.place else "Undefined place") + ', ' + str(latitude) + ', ' + str(longitude)
    count += 1
    if count > upper_limit:
        exit()
