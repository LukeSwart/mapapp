package main.java.deploy;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BoxLayout;
import javax.swing.JApplet;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.UIManager;

import main.java.visualization.InteractionPane;
import main.java.visualization.MapPane;
import main.java.visualization.USMaps;
import main.java.visualization.USMaps.Version;

public class MapApp extends JApplet {

	private static final long serialVersionUID = 8288679591523668618L;
	public static USMaps maps;
	private static JFrame appFrame;
	private static InteractionPane interactionPane;
	final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public static void main(String[] args) {
		LOGGER.log(Level.INFO, "In WebApplet MAIN method.");


		LOGGER.setLevel(Level.INFO);
		LOGGER.log(Level.INFO,
				"Logger initialized to global logger for query manager runtime.");

		try {
			LOGGER.log(Level.INFO, "In WebApplet MAIN method");
			javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
				public void run() {
					MapApp webApp = new MapApp();

					// Creates outermost frame
					appFrame = new JFrame("USA Population Density");
					// appFrame = new JLabel("USA Population Density");
					appFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

					LOGGER.log(Level.INFO, "Beginning init()...");
					webApp.init();
					LOGGER.log(Level.INFO, "Completed init()...");

					appFrame.setContentPane(webApp.getContentPane());
					appFrame.setJMenuBar(webApp.getJMenuBar());
					// Resize window based on screen size
					Dimension screen = Toolkit.getDefaultToolkit()
							.getScreenSize();
					appFrame.setSize(screen.width * 7 / 8,
							screen.height * 7 / 8);
					appFrame.setLocation(screen.width / 16,
							screen.height / 16 - 20);

					appFrame.setVisible(true);

					// according to the swing documentation, one must call
					// validate
					// after
					// adding components to a visible item -- doesn't seem to
					// matter
					// on Windows,
					// but important on Mac to avoid needing to resize manually
					// appFrame.validate();

					// Preprocess the solution
					LOGGER.log(Level.INFO, "Set app frame to visible, pre processing...");
					USMaps.pqPreprocess();
				}
			});
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "createGUI didn't successfully complete: ", e);
		}
	}

	public void init() {
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Cannot set look and feel: ", e);
			System.exit(1);
		}

		// Prepare and run separate process for solution code.
		int initX = 138; // initial X value
		int initY = 72; // initial Y value

		// Creates menu toolbar and adds to mainFrame
		JMenuBar menuToolbar = createToolbar();
		setJMenuBar(menuToolbar);
		LOGGER.log(Level.INFO, "tool bar is set...");

		// The main frame should be laid out vertically
		BoxLayout mainLayout = new BoxLayout(getContentPane(), BoxLayout.Y_AXIS);
		setLayout(mainLayout);
		LOGGER.log(Level.INFO, "Boxed layout manager is set...");

		// Creates Map Pane (map-viewing pane) and adds to mainFrame
		MapPane mapPane = USMaps.getMapPane();// MapPane extends JPanel
			mapPane = USMaps.getMapPane(); 
		LOGGER.log(Level.INFO, "Accessed the map pane");
		add(mapPane);
		LOGGER.log(Level.INFO, "Map pane is set...");

		// Creates Interaction Pane and adds to mainFrame
		InteractionPane interactionPane = new InteractionPane(this);
		LOGGER.log(Level.INFO, "Interaction pane is instantiated...");

		try {
			interactionPane.initMapGrid(initY, initX, mapPane);
		} catch (Throwable t) {
			LOGGER.log(Level.SEVERE, "Exception caught: ", t);
			t.printStackTrace();
			try {
				Thread.sleep(100000);
			} catch (InterruptedException e2) {
				LOGGER.log(Level.SEVERE, "Interrupted Exception: ", t);
			}
		}
		LOGGER.log(Level.INFO, "Interaction pane  is initialized...");

		add(interactionPane, BorderLayout.SOUTH);
	}
	
	private JMenuBar createToolbar() {
		JMenuBar toolbar = new JMenuBar();

		// The File menu
		JMenu fileMenu = new JMenu("File");
		// Exit item with Ctrl+x shortcut
		JMenuItem exitItem = new JMenuItem("Exit");
		exitItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		fileMenu.add(exitItem);

		// The Run menu
		JMenu runMenu = new JMenu("Run");
		// "Change Run" item
		JMenu changeRunSubMenu = new JMenu("Change Run");
		// "Zoom" item
		JMenu zoomMenu = new JMenu("Zoom");
		// change run to v1
		final JMenuItem changeToV1 = new JMenuItem("V.1");
		changeToV1.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1,
				Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(),
				false));
		changeToV1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				USMaps.running = USMaps.Version.ONE;
				interactionPane.deselectAllButtons();
				interactionPane.selectButton(1);
				USMaps.pqPreprocess();
				//JOptionPane.showMessageDialog(changeToV1,
				//"Changed to V1");
			}
		});
		// change run to v2
		final JMenuItem changeToV2 = new JMenuItem("V.2");
		changeToV2.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2,
				Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(),
				false));
		changeToV2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				USMaps.running = Version.TWO;
				interactionPane.deselectAllButtons();
				interactionPane.selectButton(2);
				USMaps.pqPreprocess();
				// JOptionPane.showMessageDialog(changeToV2,
				// "Changed to V2");
			}
		});
		// change run to v3
		final JMenuItem changeToV3 = new JMenuItem("V.3");
		changeToV3.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_3,
				Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(),
				false));
		changeToV3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				USMaps.running = Version.THREE;
				interactionPane.deselectAllButtons();
				interactionPane.selectButton(3);
				USMaps.pqPreprocess();
				// JOptionPane.showMessageDialog(changeToV3,
				// "Changed to V3");
			}
		});
		// change run to v4
		final JMenuItem changeToV4 = new JMenuItem("V.4");
		changeToV4.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_4,
				Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(),
				false));
		changeToV4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				USMaps.running = Version.FOUR;
				interactionPane.deselectAllButtons();
				interactionPane.selectButton(4);
				USMaps.pqPreprocess();
				// JOptionPane.showMessageDialog(changeToV4,
				// "Changed to V4");
			}
		});
		// change run to v5
		final JMenuItem changeToV5 = new JMenuItem("V.5");
		changeToV5.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_5,
				Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(),
				false));
		changeToV5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				USMaps.running = Version.FIVE;
				interactionPane.deselectAllButtons();
				interactionPane.selectButton(5);
				USMaps.pqPreprocess();
				// JOptionPane.showMessageDialog(changeToV5,
				// "Changed to V5");
			}
		});
		// Run item
		final JMenuItem runItem = new JMenuItem("Run");
		runItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G,
				Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(),
				false));
		runItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				USMaps.runProgram();
			}
		});
		final JMenuItem noZoom = new JMenuItem("None");
		noZoom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				USMaps.getMapPane().unzoom();
				// appFrame.validate();
			}
		});
		final JMenuItem zoom = new JMenuItem("Continental U.S.");
		zoom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
//				USMaps.mapPane.zoom();
				USMaps.getMapPane().zoom();
				// appFrame.validate();
			}
		});
		changeRunSubMenu.add(changeToV1);
		changeRunSubMenu.add(changeToV2);
		changeRunSubMenu.add(changeToV3);
		changeRunSubMenu.add(changeToV4);
		changeRunSubMenu.add(changeToV5);
		runMenu.add(changeRunSubMenu);
		runMenu.addSeparator();
		runMenu.add(runItem);

		zoomMenu.add(noZoom);
		zoomMenu.add(zoom);

		toolbar.add(fileMenu);
		toolbar.add(runMenu);
		toolbar.add(zoomMenu);

		return toolbar;
	}
}