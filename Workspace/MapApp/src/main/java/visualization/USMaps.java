package main.java.visualization;

import java.util.List;

import main.java.logic.InteractionHandler;
import main.java.utilities.Pair;

public class USMaps {

	public enum Version {
		ONE, TWO, THREE, FOUR, FIVE
	};

	public static Version running = Version.ONE;
	private static MapPane mapPane = new main.java.visualization.MapPane();
	private static final String CENSUS_FILENAME = "data/CenPop2010.txt";
	private static final String TWEET_FILENAME = "data/USTweets.txt";
	
	public static MapPane getMapPane() {
		return mapPane;
	}
	
	public static void runProgram() {
		int w = mapPane.getWest();
		int s = mapPane.getSouth();
		int e = mapPane.getEast();
		int n = mapPane.getNorth();
		List<Pair<Integer, Double>> queryResults = InteractionHandler.singleInteraction(w, s, e,n);
		InteractionPane.displayCensusData(queryResults.get(0).getElementA(),
				queryResults.get(0).getElementB());
		InteractionPane.displayTweetData(queryResults.get(1).getElementA(),
				queryResults.get(1).getElementB());
	}

	public static int getVersionNum() {
		switch (running) {
		case ONE:
			return 1;
		case TWO:
			return 2;
		case THREE:
			return 3;
		case FOUR:
			return 4;
		case FIVE:
			return 5;
		default:
			return -1;
		}
	}

	public static void pqPreprocess() {
		InteractionHandler.preprocess(CENSUS_FILENAME, TWEET_FILENAME, mapPane.getColumns(),
				mapPane.getRows(), getVersionNum());
	}
}