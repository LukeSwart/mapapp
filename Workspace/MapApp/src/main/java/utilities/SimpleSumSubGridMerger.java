package main.java.utilities;

import java.util.concurrent.RecursiveTask;

/*
 * Uses Fork-Join to add one grid to another, element-by-element. 
 * This is used to create a simple sum grid where each cell represents 
 * the total population for that area.
 */
public class SimpleSumSubGridMerger extends RecursiveTask<int[][]> {

	private static final long serialVersionUID = 1655792268394007344L;
	private static final int ROW_CUTOFF = 2;
	private static final int COL_CUTOFF = 2;

	int[][] left_;
	int[][] right_;
	int rowLo_;
	int rowHi_;
	int colLo_;
	int colHi_;
	
	SimpleSumSubGridMerger(int[][] left, int[][] right, int rowLo, int rowHi, int colLo, int colHi) {
		left_ = left;
		right_ = right;
		rowLo_ = rowLo;
		rowHi_ = rowHi;
		colLo_ = colLo;
		colHi_ = colHi;
	}
	
	@Override
	// Always adds the populations to the left grid.
	// TODO: IMPLEMENT THIS. 
	// Note that the 2-D arrays can be unrolled into a 1-D array, simplifying the fork-join process.
	// But would the array copying involved in unrolling an array discount any parallel performance gains? 
	protected int[][] compute() {
		if (rowHi_ - rowLo_ <= ROW_CUTOFF && colHi_ - colLo_ <= COL_CUTOFF) {
			for (int i = rowLo_; i < rowHi_; i++) {
				for (int j = colLo_; j < colHi_; j++) {
					left_[i][j] += right_[i][j];
				}
			}
//			return results_;
			return left_;
		}
		int rowMid = rowLo_ + (((rowHi_ - rowLo_) + 1) / 2); 
		int colMid = colLo_ + (((colHi_ - colLo_) + 1) / 2);
//		SimpleSumSubGridMerger topLeft = new SimpleSumSubGridMerger(results_, left_, right_, rowLo_, rowMid, colLo_, colMid);
//		SimpleSumSubGridMerger topRight = new SimpleSumSubGridMerger(results_, left_, right_, rowMid, rowHi_, colLo_, colMid);
//		SimpleSumSubGridMerger bottomLeft = new SimpleSumSubGridMerger(results_, left_, right_, rowMid, rowHi_, colLo_, colMid);
//		SimpleSumSubGridMerger bottomRight = new SimpleSumSubGridMerger(results_, left_, right_, rowMid, rowHi_, colLo_, colMid);
		SimpleSumSubGridMerger topLeft = new SimpleSumSubGridMerger(left_, right_, rowLo_, rowMid, colLo_, colMid);
		SimpleSumSubGridMerger topRight = new SimpleSumSubGridMerger(left_, right_, rowMid, rowHi_, colLo_, colMid);
		SimpleSumSubGridMerger bottomLeft = new SimpleSumSubGridMerger(left_, right_, rowMid, rowHi_, colLo_, colMid);
		SimpleSumSubGridMerger bottomRight = new SimpleSumSubGridMerger(left_, right_, rowMid, rowHi_, colLo_, colMid);
		topLeft.fork();
		topRight.fork();
		bottomLeft.fork();
		bottomRight.compute();
		topLeft.join();
		topRight.join();
		bottomLeft.join();
//		int[][] newArray = (topLeft[rowLo_:rowMid][colLo_:colMid] + topRight 
		return left_;
	}
	
//	public static int[][] sumSubGrids(int[][] left, int[][] right) {
//		// Unroll both of the 2-D arrays into two one 1-D arrays.
//		int[] unrolledLeft = 
//	}
	
	// Temporary place grid merging algorithm until a parallel version can be 
	// implemented.
	// Assumes the left and right grids have equal dimensions.
	public static int[][] mergeSubGrids(int[][] leftGrid, int[][] rightGrid) {
		// Merge the two grids by traversing the right grid.
		int[][] mergeResult = new int[leftGrid.length][leftGrid[0].length];
		for (int i = 0; i < rightGrid.length; i++) {
			for (int j = 0; j < rightGrid[0].length; j++) {
//				leftGrid[i][j] = leftGrid[i][j] + rightGrid[i][j];
				mergeResult[i][j] = leftGrid[i][j] + rightGrid[i][j];
			}
		}
		return mergeResult;
	}
}