package main.java.utilities;

import java.util.logging.Level;
import java.util.logging.Logger;

import main.java.logic.DataPoint;
import main.java.logic.QueryGrid;

/*
 * Builds as simple grid that is scaled to fit the map with each
 * cell containing the population in that section of the map.
 */
//public class CallableSimpleGridGenerator implements Callable<int[][]> {
public class RunnableSimpleGridGenerator implements Runnable {
	private static final Logger LOGGER = Logger
	.getLogger(Logger.GLOBAL_LOGGER_NAME);
	
	private int[][] grid_;
	private Object[][] locks_;
	private DataPoint[] dataPoints_;
	private int lo_;
	private int hi_;
	private QueryGrid queryGrid_;

	public RunnableSimpleGridGenerator(int[][] grid, DataPoint[] dataPoints,
										Object[][] locks, int lo, int hi,
										QueryGrid queryGrid) {

		grid_ = grid;
		dataPoints_ = dataPoints;
		lo_ = lo;
		hi_ = hi;
		locks_ = locks;
		queryGrid_ = queryGrid;
	}
	
	@Override
	public void run() {
		synchronized (MapAppLogger.LOG_LOCK) {
			LOGGER.fine("Running grid..");
			LOGGER.fine("second Logging statement.");
		}
		for (int i = lo_; i < hi_; i++) {
			DataPoint dataPoint = dataPoints_[i];
			int col = QueryGrid.getColFromGroup(dataPoint, queryGrid_);
			int row = QueryGrid.getRowFromDataPoint(dataPoint, queryGrid_);
			// Handle case when the col or row is at the North or East border.
			if (col == Integer.MAX_VALUE || row == Integer.MAX_VALUE)
				continue;
			if (col > queryGrid_.getCols() + 1) {
				
				synchronized (MapAppLogger.LOG_LOCK) {
					LOGGER.log(Level.SEVERE, "Cols do not match: " + col + 
							", " + (queryGrid_.getCols() + 1));
				}
				throw new IllegalStateException();
			}
			if (row > queryGrid_.getRows() + 1) {
				synchronized (MapAppLogger.LOG_LOCK) {
					LOGGER.log(Level.SEVERE, "Rows do not match: " + row + 
						", " + (queryGrid_.getRows() + 1));
				}
				throw new IllegalStateException();
			}
			// Update the grid.
			synchronized (locks_[col][row]) {
				if (i % 10000 == 0) {
	
					synchronized (MapAppLogger.LOG_LOCK) {
						LOGGER.log(Level.FINE, "adding index: " + i + 
								" to current grid value: " + grid_[col][row]);
					}
				}
				
				grid_[col][row] += dataPoint.population;
			}
		}
	}
//		// Combine the two grids. Use fork-join again.
//		// TODO: Implement parallelized grid merging.
////		leftGrid = Globals.fjPool.invoke(new SimpleSumSubGridMerger(leftGrid, rightGrid, 0, 0, leftGrid.length,
////				leftGrid[0].length));
////		int[][] mergedGrid = SimpleSumSubGridMerger.sumSubGrids(left, right);
//		
//		return SimpleSumSubGridMerger.mergeSubGrids(leftGrid, rightGrid);
//	}
}