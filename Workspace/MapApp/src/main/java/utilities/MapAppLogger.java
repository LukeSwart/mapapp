package main.java.utilities;

import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class MapAppLogger {
	static private FileHandler fileTxt;
	static private SimpleFormatter formatterTxt;

	// get the global logger to configure it
	private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	
	public static final Object LOG_LOCK = new Object();
	
	private static final boolean LOG_TO_CONSOLE = true;

	static public void setup() throws IOException {
		// suppress the logging output to the console
		if (!LOG_TO_CONSOLE) {
			Logger rootLogger = Logger.getLogger("");
			Handler[] handlers = rootLogger.getHandlers();
			if (handlers[0] instanceof ConsoleHandler) {
				rootLogger.removeHandler(handlers[0]);
			}
		}

		LOGGER.setLevel(Level.INFO);
		
		fileTxt = new FileHandler("log/MapAppLog.txt");
		// fileHTML = new FileHandler("Logging.html");

		// create a TXT formatter
		formatterTxt = new SimpleFormatter();
		fileTxt.setFormatter(formatterTxt);
		LOGGER.addHandler(fileTxt);
	}
}