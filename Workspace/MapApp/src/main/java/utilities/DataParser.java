package main.java.utilities;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import main.java.logic.CensusGroup;
import main.java.logic.DataPoint;
import main.java.logic.DataResults;
import main.java.logic.Tweet;

public class DataParser {
	private static final Logger LOGGER = Logger
			.getLogger(Logger.GLOBAL_LOGGER_NAME);

	// next four constants are relevant to census data parsing
	// "STATEFP,COUNTYFP,TRACTCE,BLKGRPCE,POPULATION,LATITUDE,LONGITUDE"
	public static final int CENSUS_TOKENS_PER_LINE = 7;
	// zero-based indices of the census data columns
	public static final int CENSUS_POPULATION_INDEX = 4;
	public static final int CENSUS_LATITUDE_INDEX = 5;
	public static final int CENSUS_LONGITUDE_INDEX = 6;

	// next four constants are relevant to Tweet data parsing
	// ie: TIME, TWEET_TEXT, PLACE_NAME, LATITUDE, LONGITUDE
	public static final int TWEET_TOKENS_PER_LINE = 5;
	// zero-based indices of the census data columns
	public static final int TWEET_LATITUDE_INDEX = 3;
	public static final int TWEET_LONGITUDE_INDEX = 4;

	public static DataResults getCensusResultsSequentially(String fileName) {
		// Read the file (sequentially)
		DataResults result = parseCensusData(fileName);
		return processMaxMin(result);
	}

	public static DataResults getTweetResultsSequentially(String fileName) {
		// Read the file (sequentially)
		DataResults result = parseTweetData(fileName);
		return processMaxMin(result);
	}
	
	public static DataResults processMaxMin(DataResults result) {
		// Traverse each data point, updating the max long/lat as
		// needed for each point.
		for (int i = 0; i < result.getNumberOfDataPoints(); i++) {
			DataPoint dataPoint = result.getDataPointAtIndex(i);
			result.updateMaxMinCoordinates(dataPoint);
		}
		return result;
	}

	// // public static CensusResults sequentially(CensusData data) {
	// public static CensusResults getCensusResultsSequentially(String fileName)
	// {
	// // Read the file (sequentially)
	// CensusResults result = parseCensusData(fileName);
	// // CensusGroup[] censusGroups = result.getCensusGroups();
	// // Traverse each census group, updating the max long/lat as
	// // needed for each group.
	// for (int i = 0; i < result.getNumberOfCensusGroups(); i++) {
	// CensusGroup group = result.getCensusGroupAtIndex(i);
	// result.updateMaxMinCoordinates(group);
	// }
	// return result;
	// }

	// // public static CensusResults sequentially(CensusData data) {
	// public static TweetResults getTweetResultsSequentially(String fileName) {
	// // Read the file (sequentially)
	// TweetResults result = parseTweetData(fileName);
	// // CensusGroup[] censusGroups = result.getCensusGroups();
	// // Traverse each census group, updating the max long/lat as
	// // needed for each group.
	// for (int i = 0; i < result.getNumberOfTweets(); i++) {
	// Tweet tweet = result.getTweetAtIndex(i);
	// result.updateMaxMinCoordinates(tweet);
	// }
	// return result;
	// }

	public static BufferedReader getBufferedReader(String fileName) {
		BufferedReader fileIn = null;
		try {
			LOGGER.log(Level.INFO, "Reading input file: " + fileName);
			LOGGER.log(Level.INFO,
					"Logged complete,\nreading census file now....");

			InputStream fileStream = null;
			try {
				// NOTE: ClassLoader loads files from project "bin/" folder.
				GenerateDataResultsInParallel.class.getClassLoader();
				URL urlFileStream = GenerateDataResultsInParallel.class
						.getClassLoader().getResource(fileName);
				LOGGER.log(Level.FINE, "Read file file name as URL: "
						+ urlFileStream);
				fileStream = urlFileStream.openStream();
				LOGGER.log(Level.FINE,
						"Read file input stream as URL Classloader: "
								+ fileStream);
				fileIn = new BufferedReader(new InputStreamReader(fileStream));
				LOGGER.log(Level.FINE, "Read file buffered reader: " + fileIn);
			} catch (Throwable t) {
				LOGGER.log(Level.SEVERE, "Cannot load buffered reader: " + t);
			}
			if (fileStream == null) {
				LOGGER.log(Level.SEVERE, "fileStream is null, returning null.");
				return null;
			}
			if (fileIn == null) {
				LOGGER.log(Level.SEVERE, "fileIn is null, returning null.");
				return null;
			}
			LOGGER.log(Level.INFO, "Loaded buffered reader.");

		} catch (NumberFormatException nfe) {
			LOGGER.log(Level.SEVERE, nfe.toString(), nfe);
			System.exit(1);
			return null;
		}
		return fileIn;
	}

	// parse the input file into a large array held in a CensusData object
	public static DataResults parseCensusData(String fileName) {
		DataResults result = new DataResults();
		BufferedReader fileIn = getBufferedReader(fileName);
		try {
			if (fileIn == null)
				throw new FileNotFoundException("Can not find file at: " + fileName);
			String oneLine = fileIn.readLine();
			// Skip the first line of the file
			// After that each line has 7 comma-separated numbers (see constants
			// above)
			// We want to skip the first 4, the 5th is the population (an int)
			// and the 6th and 7th are latitude and longitude (floats)
			// If the population is 0, then the line has latitude and longitude
			// of +.,-.
			// which cannot be parsed as floats, so that's a special case
			// (we could fix this, but noisy data is a fact of life, more fun
			// to process the real data as provided by the government)

			// ie
			// "STATEFP,COUNTYFP,TRACTCE,BLKGRPCE,POPULATION,LATITUDE,LONGITUDE"

			// read each subsequent line and add relevant data to a big array
			while ((oneLine = fileIn.readLine()) != null) {
				// The "-1" allows any number of fields,
				// not discarding any trailing fields.
				String[] tokens = oneLine.split(",", -1);
				if (tokens.length != CENSUS_TOKENS_PER_LINE) {
					fileIn.close();
					LOGGER.log(Level.SEVERE,
							"Tokens per line does not match. Should be: "
									+ CENSUS_TOKENS_PER_LINE + " but is: "
									+ tokens.length);
					throw new NumberFormatException();
				}
				int population = Integer
						.parseInt(tokens[CENSUS_POPULATION_INDEX]);
				// if (population != 0) { // skip groups with populations of "0"
				CensusGroup newGroup = new CensusGroup(population,
						Float.parseFloat(tokens[CENSUS_LATITUDE_INDEX]),
						Float.parseFloat(tokens[CENSUS_LONGITUDE_INDEX]));
				result.add(newGroup);
				// }
			}
			fileIn.close();
		} catch (IOException ioe) {
			LOGGER.log(Level.SEVERE, ioe.toString(), ioe);
			System.exit(1);
		}
		return result;
	}

	public static DataResults parseTweetData(String fileName) {
		DataResults result = new DataResults();
		BufferedReader fileIn = getBufferedReader(fileName);
		try {

			String oneLine = fileIn.readLine();

			// ie
			// TIME, TWEET_TEXT, PLACE_NAME, LATITUDE, LONGITUDE

			// read each subsequent line and add relevant data to a big array
			while ((oneLine = fileIn.readLine()) != null) {
				// The "-1" allows any number of fields,
				// not discarding any trailing fields.
				// Commas have been removed from text field during
				// pre-processing.
				String[] tokens = oneLine.split(", ", -1);
				if (tokens.length != TWEET_TOKENS_PER_LINE) {
					fileIn.close();
					LOGGER.log(Level.INFO,
							"Tokens per line does not match. Should be: "
									+ TWEET_TOKENS_PER_LINE + " but is: "
									+ tokens.length);
					LOGGER.log(Level.INFO,
							"Token is: \n" + Arrays.toString(tokens));
					throw new IllegalStateException();
				}
				// Each tweet has a population of 1.
				// This can be useful for processing NLP, etc.
				int population = 1;
				// skip tweets with unknown latitude or longitude.
				if (!tokens[TWEET_LATITUDE_INDEX].equals("Undefined latitude")
						&& !tokens[TWEET_LONGITUDE_INDEX]
								.equals("Undefined longitude")) {
					Tweet newTweet = new Tweet(population,
							Float.parseFloat(tokens[TWEET_LATITUDE_INDEX]),
							Float.parseFloat(tokens[TWEET_LONGITUDE_INDEX]));
					result.add(newTweet);
				}
			}
			fileIn.close();
		} catch (IOException ioe) {
			LOGGER.log(Level.SEVERE, ioe.toString(), ioe);
			System.exit(1);
		} catch (NumberFormatException nfe) {
			LOGGER.log(Level.SEVERE, nfe.toString(), nfe);
			System.exit(1);
		}
		return result;
	}
}