package main.java.utilities;

import java.util.concurrent.RecursiveTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import main.java.logic.DataPoint;
import main.java.logic.DataResults;

/*
 * Utility class that performs operation on the census data.
 */
public class GenerateDataResultsInParallel extends RecursiveTask<DataResults> {
	private static final Logger LOGGER = Logger
			.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public static final int DATA_POINT_THRESHOLD = 1;
//
//	// next four constants are relevant to parsing
//	public static final int TOKENS_PER_LINE = 7;
//	// zero-based indices
//	public static final int POPULATION_INDEX = 4;
//	public static final int LATITUDE_INDEX = 5;
//	public static final int LONGITUDE_INDEX = 6;
//
	/**
     * 
     */
	private static final long serialVersionUID = 3258280377158428654L;
//	private static final boolean DEBUG = true;

	private DataPoint[] arr;
	private int lo;
	private int hi;

	// [lo, hi), inclusive for lo, exclusive for hi
	GenerateDataResultsInParallel(DataPoint[] arr, int lo, int hi) {
		this.arr = arr;
		this.lo = lo;
		this.hi = hi;
	}

	@Override
	protected DataResults compute() {
		if (hi - lo <= DATA_POINT_THRESHOLD) {
			DataResults results = new DataResults();
			for (int i = lo; i < hi; i++) {
				DataPoint dataPoint = arr[i];
				// results.addGroupAndUpdateMaxMin(group);
				// results.add(group);
				results.updateMaxMinCoordinates(dataPoint);
			}
			return results;
		}
		int mid = lo + (((hi - lo) + 1) / 2);
		GenerateDataResultsInParallel left = new GenerateDataResultsInParallel(arr, lo, mid);
		GenerateDataResultsInParallel right = new GenerateDataResultsInParallel(arr, mid,
				hi);
		left.fork();
		DataResults rResults = right.compute();
		DataResults lResults = left.join();
		lResults.merge(rResults);
//		maxLat = Math.max(maxLat, otherResults.maxLat);
//		minLat = Math.min(minLat, otherResults.minLat);
//		maxRealLat = Math.max(maxRealLat, otherResults.maxRealLat);
//		minRealLat = Math.min(minRealLat, otherResults.minRealLat);
//		maxLon = Math.max(maxLon, otherResults.maxLon);
//		minLon = Math.min(minLon, otherResults.minLon);
//		total = total + otherResults.total;

		return lResults;
	}

	public static DataResults getCensusResultsInParallel(String fileName) {
		// Read the file (sequentially)
		DataResults results = DataParser.parseCensusData(fileName);
		// Update the max and min latitudes and longitudes
		LOGGER.log(Level.INFO, "parsed census data for results: " + results
				+ "\ncomputing boundaries in parallel....");
		return processDataResultsInParallel(results);
	}
	
	public static DataResults getTweetResultsInParallel(String fileName) {
		// Read the file (sequentially)
		DataResults results = DataParser.parseTweetData(fileName);
		// Update the max and min latitudes and longitudes
		LOGGER.log(Level.INFO, "parsed tweet data for results: " + results
				+ "\ncomputing boundaries in parallel....");
		return processDataResultsInParallel(results);
	}
	
	public static DataResults processDataResultsInParallel(DataResults results) {
		DataResults mergedResults = Globals.fjPool.invoke(new GenerateDataResultsInParallel(results
				.getDataPoints(), 0, results.getNumberOfDataPoints()));
		for (int i = 0; i < results.getNumberOfDataPoints(); i++)// cannot use foreach loop of array without proper sizing
			mergedResults.add(results.getDataPointAtIndex(i));
		return mergedResults;
	}
}