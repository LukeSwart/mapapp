package main.java.utilities;

import java.util.concurrent.RecursiveTask;

import main.java.logic.DataPoint;
import main.java.logic.DataResults;
import main.java.logic.QueryGrid;

// Create the modified sum grid, where each grid element holds the total for
// all positions
// that are neither farther East nor farther South.
// In other words, grid element g stores the total population in the
// rectangle whose
// upper-left is the North-West corner of the country and the lower-right
// corner is g.
public class ForkJoinSimpleGridGenerator extends RecursiveTask<int[][]> {
//	private static final Logger LOGGER = Logger
//			.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private static final long serialVersionUID = -258590034797698392L;
	// census group length cutoff.
	private static final int CUTOFF = 100;

	private DataResults dataResults_;
	private DataPoint[] dataPoints_;
	int lo_;
	int hi_;
	QueryGrid queryGrid_;

		ForkJoinSimpleGridGenerator(DataResults dataResults, QueryGrid queryGrid,
				int lo, int hi) {
		// grid_ = new int[gridBoundaries.rows + 1][gridBoundaries.cols + 1];
//		grid_ = grid;
		queryGrid_ = queryGrid;
		dataResults_ = dataResults;
		dataPoints_ = dataResults.getDataPoints();
//		gridBoundaries_ = gridBoundaries;
		// cols_ = gridBoundaries.cols;
		// rows_= gridBoundaries.rows;
//		rowHeight_ = rowHeight;
//		colLength_ = colLength;
		lo_ = lo;
		hi_ = hi;
	}

	@Override
	protected int[][] compute() {
		if (hi_ - lo_ <= CUTOFF) {
			// Generate a new grid to comply with version 4.
			int[][] grid = new int[queryGrid_.getCols() + 1][queryGrid_.getRows() + 1];
			// Add the remaining cutoff groups to this instances array.
			for (int i = lo_; i < hi_; i++) {
				DataPoint dataPoint = dataPoints_[i];
				QueryGrid.addDataPointPopulationToGrid(dataPoint, queryGrid_, grid);
			}
			return grid;
		}
		int mid = lo_ + (((hi_ - lo_) + 1) / 2);
		// SimpleSumGridGenerator left = new
		// SimpleSumGridGenerator(censusResults_, gridBoundaries_, lo_, mid);
		// SimpleSumGridGenerator right = new
		// SimpleSumGridGenerator(censusResults_, gridBoundaries_, mid, hi_);
		ForkJoinSimpleGridGenerator left = new ForkJoinSimpleGridGenerator(
				dataResults_, queryGrid_, lo_, mid);
		ForkJoinSimpleGridGenerator right = new ForkJoinSimpleGridGenerator(
				dataResults_, queryGrid_, mid, hi_);
		left.fork();
		int[][] rightGrid = right.compute();
		int[][] leftGrid = left.join();
		// Combine the two grids. Use fork-join again.
		// TODO: Implement parallelized grid merging.
//		leftGrid = Globals.fjPool.invoke(new SimpleSumSubGridMerger(leftGrid, rightGrid, 0, 0, leftGrid.length,
//				leftGrid[0].length));
//		int[][] mergedGrid = SimpleSumSubGridMerger.sumSubGrids(left, right);
		
		return SimpleSumSubGridMerger.mergeSubGrids(leftGrid, rightGrid);
	}

	public static int[][] getSimpleSumGrid(DataResults dataResults, QueryGrid queryGrid) {

		return Globals.fjPool.invoke(new ForkJoinSimpleGridGenerator(dataResults, queryGrid,
				0, dataResults.getNumberOfDataPoints()));
	}
}