package main.java.utilities;

import java.util.concurrent.RecursiveTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import main.java.logic.CoordinateRectangle;
import main.java.logic.DataPoint;
import main.java.logic.DataResults;

/*
 * Finds the population within a query rectangle based on an approach
 * that traverses through all census groups.
 * Uses Fork-Join framework to parallelize the traversal. 
 */
public class SimpleDataPointPopulationQueryParallel extends RecursiveTask<Integer> {

	private static final long serialVersionUID = -7099351464661451746L;
	private static final int THRESHOLD = 50;
    private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	DataPoint[] arr;
	int lo;
	int hi;
	int population;
	CoordinateRectangle rect;

	SimpleDataPointPopulationQueryParallel(DataPoint[] arr, int lo, int hi,
			int population, CoordinateRectangle rect) {
		this.arr = arr;
		this.lo = lo;
		this.hi = hi;
		this.population = population;
		this.rect = rect;
	}

	@Override
	// Computes the sum of group populations from [lo, hi) within the rectangle.
	// We do not include the "hi" index in our calculation - it is out of the array's boundaries.
	protected Integer compute() {
		if (hi - lo <= THRESHOLD) {
			int populationCount = 0;
			for (int i = lo; i < hi; i++) {
				DataPoint dataPoint = arr[i];
				if (rect.contains(dataPoint)) {
					populationCount += dataPoint.getPopulation();
				}
			}
			return populationCount;
		}
		int mid = lo + (((hi - lo) + 1) / 2);
		SimpleDataPointPopulationQueryParallel left = new SimpleDataPointPopulationQueryParallel(
				arr, lo, mid, population, rect);
		SimpleDataPointPopulationQueryParallel right = new SimpleDataPointPopulationQueryParallel(
				arr, mid, hi, population, rect);
		left.fork();
		int rPop = right.compute();
		int lPop = left.join();
		return rPop + lPop;
	}

	public static Integer find(DataResults dataResults,
			CoordinateRectangle rect) {
		LOGGER.log(Level.INFO, "invoking fork-join on low: 0, and hi: " + dataResults.getNumberOfDataPoints());
		return Globals.fjPool.invoke(new SimpleDataPointPopulationQueryParallel(
				dataResults.getDataPoints(), 0, dataResults
						.getNumberOfDataPoints(), 0, rect));
	}
}