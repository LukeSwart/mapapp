package main.java.utilities;

import main.java.logic.CoordinateRectangle;
import main.java.logic.DataPoint;
import main.java.logic.DataResults;
import main.java.logic.GridBoundaries;
import main.java.logic.GridRectangle;
import main.java.logic.QueryGrid;

public class MapUtilities {
	public static final double EPSILON = .02;
//	private static final Logger LOGGER = Logger
//			.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private MapUtilities() {
	}; // static class

	// Converts grided rectangle to a mercator
	// coordinate based rectangle
	public static CoordinateRectangle convertToCoordinateRect(
			GridRectangle rect, QueryGrid queryGrid) {
		double n = northernRowToMercatorLatitude(rect.top_, queryGrid.getRows(),
				queryGrid.getMinLat(), queryGrid.getMaxLat());
		double e = easternColToRealLongitude(rect.right_, queryGrid.getCols(),
				queryGrid.getMinLon(), queryGrid.getMaxLon());
		double s = southernRowToMercatorLatitude(rect.bottom_, queryGrid.getRows(),
				queryGrid.getMinLat(), queryGrid.getMaxLat());
		double w = westernColToRealLongitude(rect.left_, queryGrid.getCols(),
				queryGrid.getMinLon(), queryGrid.getMaxLon());
//		double n = northernRowToRealLatitude(rect.top_, queryGrid.getRows(),
//				queryGrid.getMinLat(), queryGrid.getMaxLat());
//		double e = easternColToRealLongitude(rect.right_, queryGrid.getCols(),
//				queryGrid.getMinLon(), queryGrid.getMaxLon());
//		double s = southernRowToRealLatitude(rect.bottom_, queryGrid.getRows(),
//				queryGrid.getMinLat(), queryGrid.getMaxLat());
//		double w = westernColToRealLongitude(rect.left_, queryGrid.getCols(),
//				queryGrid.getMinLon(), queryGrid.getMaxLon());
		return new CoordinateRectangle(w, e, n, s);
//			MercatorGridRectangle rect, CensusResults censusResults,
//			GridBoundaries gridBoundaries) {
//		double n = northernRowToRealLatitude(rect.top, gridBoundaries.rows,
//				censusResults.minLat, censusResults.maxLat);
//		double e = easternColToRealLongitude(rect.right, gridBoundaries.cols,
//				censusResults.minLon, censusResults.maxLon);
//		double s = southernRowToRealLatitude(rect.bottom, gridBoundaries.rows,
//				censusResults.minLat, censusResults.maxLat);
//		double w = westernColToRealLongitude(rect.left, gridBoundaries.cols,
//				censusResults.minLon, censusResults.maxLon);
//		return new CoordinateRectangle(w, e, n, s);
	}

	// Given min/max mercator latitudes, number of rows, and southern grid
	// position, return the
	// southern real latitude.
	public static double southernRowToRealLatitude(int southRow, int rows,
			double minLat, double maxLat) {
		double latDiff = maxLat - minLat;
		return calcGudermann(((southRow - 1) / (double) rows) * latDiff + minLat);
	}
	public static double southernRowToMercatorLatitude(int southRow, int rows,
			double minLat, double maxLat) {
		double latDiff = maxLat - minLat;
		return ((southRow - 1) / (double) rows) * latDiff + minLat;
	}

	// Given min/max mercator latitudes, number of rows, and northern grid
	// position, return the
	// northern real latitude.
	public static double northernRowToRealLatitude(int northRow, int rows,
			double minLat, double maxLat) {
		double latDiff = maxLat - minLat;
		return calcGudermann((northRow / (double) rows) * latDiff + minLat);
	}
	public static double northernRowToMercatorLatitude(int northRow, int rows,
			double minLat, double maxLat) {
		double latDiff = maxLat - minLat;
		return (northRow / (double) rows) * latDiff + minLat;
	}

	// Given min/max mercator longitudes, number of cols, and western grid
	// position, return the
	// eastern real latitude.
	public static double westernColToRealLongitude(int westCol, int cols,
			double minLon, double maxLon) {
		double lonDiff = maxLon - minLon;
		return ((westCol - 1) / (double) cols) * lonDiff + minLon;
	}

	// Given min/max mercator longitudes, number of cols, and eastern grid
	// position, return the
	// eastern real latitude.
	public static double easternColToRealLongitude(int eastCol, int cols,
			double minLon, double maxLon) {
		double lonDiff = maxLon - minLon;
		return (eastCol / (double) cols) * lonDiff + minLon;
	}

	// Given the census group and the grid, add the group to the 
	// proper row and column of the grid.
	public static void addGroupPopulationToGrid(DataPoint dataPoint,
			DataResults dataResults, GridBoundaries gridBoundaries,
			double rowHeight, double colLength, int iteration, int[][] grid) {
		 int row = (int) Math.floor((dataPoint.latitude - dataResults.minLat) /
		 rowHeight) + 1;
		 int col = (int) Math.floor(((dataPoint.longitude - dataResults.minLon))
		 / colLength) + 1;// Note that lon is negative for all of North America
		try {
			// Edge case...ignore data points on the northern and eastern most
			// edges of the grid - see the "tie-breaking" explanation above.
			if (row == gridBoundaries.rows + 1) // row is at the top of the grid
												// - usually row is equal to the
												// max latitude.
			{
				System.out.println("Row edge case: row: " + row + "\ti: "
						+ iteration + "\tlatitude: " + dataPoint.realLatitude
						+ "\tlongitude: " + dataPoint.longitude);
				row--;
				return;
			}
			if (col == gridBoundaries.cols + 1) // col is at right of the grid -
												// usual col is equal to min
												// longitude.
			{
				System.out.println("Col edge case: col: " + col + "\ti: "
						+ iteration + "\tlatitude: " + dataPoint.realLatitude
						+ "\tlongitude: " + dataPoint.longitude);
				col--;
				return;
			}
			grid[col][row] += dataPoint.population; // Rows and cols are corrected for 1-based indexing
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println();
			System.out.println("MapUtilities Error: total (row, col): ("
					+ gridBoundaries.rows + ", " + gridBoundaries.cols + ")");
			System.out.println("MapUtilities Error: current (row, col): ("
					+ row + ", " + col + ")");
			System.out.println("MapUtilities Error: current (lat, lon): ("
					+ dataPoint.latitude + ", " + dataPoint.longitude + ")");
			System.out.println("MapUtilities Error: (maxRealLat, maxLon): ("
					+ dataResults.maxRealLat + ", " + dataResults.maxLon
					+ ")");
			System.out.println("MapUtilities Error: (minRealLat, minLon): ("
					+ dataResults.minRealLat + ", " + dataResults.minLon
					+ ")");
			System.out.println("MapUtilities Error: data.data[i]: " + dataPoint);
			System.out.println();
			throw e;
		}
	}
	
	// Get the length of the map in Mercator latitude and longitude.
	// Assumes that max/min lat/lon are the max/min longitudes/latitudes of the
	// whole map area.
	public static double getRowHeight(DataResults dataResults, GridBoundaries gridBoundaries) {
		double rowSpan = dataResults.maxLat - dataResults.minLat;
		double rowHeight = rowSpan / gridBoundaries.rows; // height of each row.
		return rowHeight;
	}
	public static double getColLength(DataResults dataResults, GridBoundaries gridBoundaries) {
		double colSpan = dataResults.maxLon - dataResults.minLon;
		double colLength = colSpan / gridBoundaries.cols; // length of each col.
		return colLength;
	}

//	// Returns a grid that is formatted to optimized a search,
//	// using the [cols][rows] as indices, where the cols and rows are taken from
//	// the MapResults.
//	// This grid contains the total population inside each row/column square
//	// unit.
//	// In the unlikely case that a census-block-group falls exactly
//	// on the border of more than one grid position, "tie-break" by
//	// assigning it to the North and/or East.
//	public static int[][] generateGrid_Sequentially(
//			GridBoundaries gridBoundaries, CensusResults censusResults) {
//		// Create a grid where each block in parsed dataset belongs to a
//		// bucket.
//		// Index at 1 for "user friendliness": 0 row/col indices are not to be
//		// used (have 0 population).
//		int[][] grid = new int[gridBoundaries.cols + 1][gridBoundaries.rows + 1];
////		// Get the length of the map in Mercator latitude and longitude.
////		// Assumes max/min lat/lon are the max/min longitudes/latitudes of the
////		// whole map area.
////		double rowSpan = censusResults.maxLat - censusResults.minLat;
////		double colSpan = censusResults.maxLon - censusResults.minLon;
////		double rowHeight = rowSpan / gridBoundaries.rows; // height of each row.
////		double colLength = colSpan / gridBoundaries.cols; // length of each col.
//		double rowHeight = getRowHeight(censusResults, gridBoundaries);
//		double colLength = getColLength(censusResults, gridBoundaries);
//
//		// System.out.println("MapUtilities: getGridSequentially. ");
//		// Add population of each block into the row/col of the grid.
//		for (int i = 0; i < censusResults.getNumberOfGroups(); i++) {
//			// round up the col/row numbers to break ties to the North/East.
//			// row, col are indexed at 0.
//			CensusGroup currentCensusGroup = censusResults.getGroupAtIndex(i);
//			addGroupPopulationToGrid(currentCensusGroup, censusResults, gridBoundaries, rowHeight, colLength, i, grid);
//		}
//		System.out.println("MapUtilities: grid[1][1]: " + grid[1][1]);
//		System.out.println("MapUtilities: grid[1][2]: " + grid[1][2]);
//		System.out.println("MapUtilities: grid[2][1]: " + grid[2][1]);
//		return grid;
//	}

//	// Create the modified sum grid, where each grid element holds the total for
//	// all positions
//	// that are neither farther East nor farther South.
//	// In other words, grid element g stores the total population in the
//	// rectangle whose
//	// upper-left is the North-West corner of the country and the lower-right
//	// corner is g.
//	public static int[][] generateModifiedSumGrid_Sequentially(
//			GridBoundaries gridBoundaries, CensusResults censusResults) {
//		int[][] grid = generateGrid_Sequentially(gridBoundaries, censusResults);
//		return formatGridToModifiedSumSequentially(grid, gridBoundaries);
//	}

//	// Takes in a grid where each grid cell holds an int representing the
//	// population in
//	// that grid cell.
//	// Returns a modified sum grid.
//	private static int[][] formatGridToModifiedSumSequentially(int[][] grid,
//			GridBoundaries gridBoundaries) {
//		// Update the grid according to the "modified sum" format:
//		// "new grid[i][j]" = "original grid[i][j]" + grid[i-1][j] +
//		// grid[i][j+1] - grid[i-1][j+1]
//		// Indexed at 1, "summing" from top to bottom, left to right.
//		for (int i = 1; i <= gridBoundaries.cols; i++) { // cols
//			for (int j = gridBoundaries.rows; j > 0; j--) { // rows
//				if (i > 1) // Only count the W block if not off the grid (i=0 is
//							// not on the grid).
//					grid[i][j] += grid[i - 1][j];
//				if (j < gridBoundaries.rows) // Only count the N block if not
//												// off the grid (j=map.rows + 1
//												// is not on the grid).
//					grid[i][j] += grid[i][j + 1];
//				if (i > 1 && j < gridBoundaries.rows) // Only count the NW block
//														// if not off the grid.
//					grid[i][j] -= grid[i - 1][j + 1];
//			}
//		}
//		return grid;
//	}

//	// For Version 3 and up.
//	public static int[][] generateModifiedSumGrid_Parallel(
//			GridBoundaries gridBoundaries, CensusResults censusResults) {
//		int[][] grid = SimpleSumGridGenerator.getSimpleSumGrid(censusResults, gridBoundaries);
//		LOGGER.log(Level.INFO, "Generated simple sum grid in parallel: ", Arrays.toString(grid));
//		return formatGridToModifiedSumSequentially(grid, gridBoundaries);
//	}

	// // Returns the row given the Mercator latitude, max/min real latitudes,
	// and number of rows.
	// public static int getRowFromLatitude(double lat, int rows, double minLat,
	// double maxLat) {
	// double rowSpan = maxLat - minLat;
	// double rowHeight = rowSpan / rows;
	// return (int) Math.floor(mercatorConversion(lat) / rowHeight) + 1;
	// }

	// Converts the GridRectangle from an equal area projection grid to a real
	// coordinate based
	// CoordinateRectangle. FOR TESTING.
	public static CoordinateRectangle convertToCoordinateRectScaledProjection(
			GridRectangle rect, DataResults dataResults,
			GridBoundaries mapResults) {
		// System.out.println("MapUtilities: mapResults: " + mapResults);
		System.out.println("MapUtilities: censusResults: " + dataResults);
		// Convert the grid rectangle into a coordinate rectangle.
		double latDiff = dataResults.maxRealLat - dataResults.minRealLat;
		double lonDiff = dataResults.maxLon - dataResults.minLon;
		double s = ((rect.bottom_ - 1) / mapResults.rows) * latDiff
				+ dataResults.minRealLat;
		double e = (rect.right_ / mapResults.cols) * lonDiff
				+ dataResults.minLon;
		double w = ((rect.left_ - 1) / mapResults.cols) * lonDiff
				+ dataResults.minLon;
		double n = (rect.top_ / mapResults.rows) * latDiff
				+ dataResults.minRealLat;
		CoordinateRectangle newRect = new CoordinateRectangle(w, e, n, s);
		return newRect;
	}

	// Converts grided rectangle from Mercator projection grid to a real
	// coordinate based rectangle.
	// FOR TESTING ONLY.
	public static GridRectangle convertToGridRect(
			CoordinateRectangle rect, DataResults dataResults,
			GridBoundaries mapResults) {
		int l = getColFromLongitude(rect.west_, mapResults.cols,
				dataResults.minLon, dataResults.maxLon);
		int r = getColFromLongitude(rect.east_, mapResults.cols,
				dataResults.minLon, dataResults.maxLon);
		int t = getRowFromRealLatitude(rect.north_, mapResults.rows,
				dataResults.minLat, dataResults.maxLat);
		int b = getRowFromRealLatitude(rect.south_, mapResults.rows,
				dataResults.minLat, dataResults.maxLat);
		return new GridRectangle(l, r, t, b);
	}

	// Returns the row given the real latitude (angle), max/min real latitudes,
	// and number of rows.
	// FOR TESTING.
	public static int getRowFromRealLatitude(double realLat, int rows,
			double minRealLat, double maxRealLat) {
		double rowSpan = mercatorConversion(maxRealLat)
				- mercatorConversion(minRealLat);
		double rowHeight = rowSpan / rows;
		return (int) Math.floor(mercatorConversion(realLat) / rowHeight) + 1;
	}

	// Returns the column given the longitude, max/min longitudes, and number of
	// cols. FOR TESTING.
	public static int getColFromLongitude(double lon, int cols, double minLon,
			double maxLon) {
		double colSpan = maxLon - minLon;
		double colHeight = colSpan / cols;
		return (int) Math.floor(lon / colHeight) + 1;
	}

	// Returns the row's top edge's real latitude given the real latitude
	// (angle), max/min real
	// latitudes, and number of rows. FOR TESTING.
	public static double getRowTopFromRealLatitude(double realLat, int row,
			int rows, double minRealLat, double maxRealLat) {
		double rowSpan = mercatorConversion(maxRealLat)
				- mercatorConversion(minRealLat);
		double rowHeight = rowSpan / rows;
		return calcGudermann(row * rowHeight);// row indexed at 1.
	}

	// This is the inverse of the Mercator equation.
	// Given the delta y, returns the angle in degrees. FOR TESTING.
	public static double calcGudermann(double y) {
		double latpi = (double) ((2 * Math.atan(Math.exp(y))) - (Math.PI / 2));
		return (double) Math.toDegrees(latpi);
	}

	// This is the actual Mercator Equation.
	public static double mercatorConversion(double lat) {
		double latpi = (double) Math.toRadians(lat);
		double x = (double) Math.log(Math.tan(latpi) + 1 / Math.cos(latpi));
		return x;
	}
}