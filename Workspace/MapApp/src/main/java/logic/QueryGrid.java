package main.java.logic;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import main.java.utilities.ForkJoinSimpleGridGenerator;
import main.java.utilities.RunnableSimpleGridGenerator;

/*
 * Contains the data structures for queries, including grid-based queries
 * and modified-sum grid-based queries.
 */
public class QueryGrid {

	private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    private int rows_;
    private int cols_;
    
    // Where each cell contains the population in that cell on an overlying map.
    // TODO: Add locks for grid data structure
    private int[][] grid_;
    // Instead of each grid element holding the total for that position, it 
    // instead holds the total for all positions that are neither farther East 
    // nor farther South.
    private int[][] modifiedSumGrid_;
    private final Object[][] locks_;
    
	private double minLat_;
    private double minRealLat_;
    private double maxLat_;
    private double maxRealLat_;
    private double minLon_;
    private double maxLon_;
    
    private double rowHeight_; // row height in mercator degrees latitude.
    private double colLength_; // col length in degrees longitude.

	public QueryGrid (int rows, int cols, DataResults censusResults) {
        rows_ = rows;
        cols_ = cols;
        // Assumes a grid based on census data boundaries.
        minLat_ = censusResults.getMinLat();
        maxLat_ = censusResults.getMaxLat();
        minLon_ = censusResults.getMinLon();
        maxLon_ = censusResults.getMaxLon();
        minRealLat_ = censusResults.getMinRealLat();
        maxRealLat_ = censusResults.getMaxRealLat();
        
        rowHeight_ = calculateRowHeight();
        colLength_ = calculateColLength();
		grid_ = new int[cols_ + 1][rows_ + 1];
		
	    locks_ = new Object[cols_ + 1][rows_ + 1];
	    for (int i = 0; i < cols_ + 1; i++) {
	    	for (int j = 0; j < rows_ + 1; j++) {
	    		locks_[i][j] = new Object();
	    	}
	    }

    }
    
	// Get the length of the map in Mercator latitude and longitude.
	// Assumes that we are stretching the grid across the max/min
    // longitudes and latitudes of the entire mapped area.
	private double calculateRowHeight() {
		double rowSpan = maxLat_ - minLat_;
		double rowHeight = rowSpan / rows_; // height of each row.
		return rowHeight;
	}
	private double calculateColLength() {
		double colSpan = maxLon_ - minLon_;
		double colLength = colSpan / cols_; // length of each col.
		return colLength;
	}
	
	public int[][] generateModifiedSumGrid_Sequentially(DataResults dataResults) {
		grid_ = generateGrid_Sequentially(dataResults);
		return formatGridToModifiedSumSequentially(); 
	}
	
	// For Versions 4 and 5.
	public int[][] generateModifiedSumGrid_Parallel(DataResults dataResults) {
		grid_ = ForkJoinSimpleGridGenerator.getSimpleSumGrid(dataResults, this);
		return formatGridToModifiedSumSequentially();
	}

	// For Version 5.
	// Assumes that the number of data groups to be processed is higher than the number of threads.  
	public int[][] generateElementGrid_LockBased(DataResults dataResults, int nthreads) throws InterruptedException {
		int totalGroups = dataResults.getNumberOfDataPoints();
	    ExecutorService executor = Executors.newFixedThreadPool(nthreads);
	    int stepSize = totalGroups / nthreads;
	    int[][] grid = new int[cols_ + 1][rows_ + 1];// Mutable shared memory result
	    for (int i = 0; i < totalGroups; i+= stepSize) { // Divide groups equally among all threads.
	    	int lo = i;
	    	int hi = i + stepSize > totalGroups ? totalGroups : i + stepSize; 
	    	LOGGER.log(Level.FINE, "Adding groups in index range to lock-based grid: from i= " + lo +
	    			" to " + hi);
	    	Runnable worker = new RunnableSimpleGridGenerator(grid, dataResults.getDataPoints(),
	    			locks_, lo, hi, this);
//	    	Callable<int[][]> worker = new CallableSimpleGridGenerator(grid, censusResults.getCensusGroups(),
//	    			lockGrid, lo, hi, this);
//	    	lockGrid, 0, censusResults.getNumberOfGroups()); // used for map-reduce recursive style.
	    	executor.execute(worker);
//	      Future<int[][]> submitGrid = executor.submit(worker);
//	      list.add(submitGrid);
	    }
//	    // now merge the results
//	    for (Future<int[][]> future : list) {
//	      try {
//	        sum += future.get();
//	      } catch (InterruptedException e) {
//	        e.printStackTrace();
//	      } catch (ExecutionException e) {
//	        e.printStackTrace();
//	      }
//	    }
//	    System.out.println(sum);
	    executor.shutdown();
	    // Wait until all threads are finish
	    executor.awaitTermination(10, TimeUnit.SECONDS);
	    grid_ = formatGridToModifiedSumSequentially();
	    return grid;
	}
	
	public void generateModifiedSumGrid_LockBased(DataResults dataResults, int nthreads) throws InterruptedException {
		grid_ = generateElementGrid_LockBased(dataResults, nthreads);
		formatGridToModifiedSumSequentially();
	}
	
	// Create only the element grid 
	// For TESTING
	public int[][] generateElementGrid_Sequentially(DataResults dataResults) {
		return generateGrid_Sequentially(dataResults);
	}

	// Create only the element grid 
	// For TESTING
	public int[][] generateElementGrid_Parallel(DataResults dataResults) {
		return ForkJoinSimpleGridGenerator.getSimpleSumGrid(dataResults, this);
	}
	
	// Returns a grid that is formatted to optimized a search,
	// using the [cols][rows] as indices, where the cols and rows are taken from
	// the MapResults.
	// This grid contains the total population inside each row/column square
	// unit.
	// In the unlikely case that a census-block-group falls exactly
	// on the border of more than one grid position, "tie-break" by
	// assigning it to the North and/or East.
	public int[][] generateGrid_Sequentially(DataResults dataResults) {
		// Create a grid where each block in parsed dataset belongs to a
		// bucket.
		// Index at 1 for "user friendliness": 0 row/col indices are not to be
		// used (have 0 population).
		grid_ = new int[cols_ + 1][rows_ + 1];
		// // Get the length of the map in Mercator latitude and longitude.
		// // Assumes max/min lat/lon are the max/min longitudes/latitudes of
		// the
		// // whole map area.
		// double rowSpan = censusResults.maxLat - censusResults.minLat;
		// double colSpan = censusResults.maxLon - censusResults.minLon;
		// double rowHeight = rowSpan / gridBoundaries.rows; // height of each
		// row.
		// double colLength = colSpan / gridBoundaries.cols; // length of each
		// col.

		// LOGGER.log(Level.INFO, "MapUtilities: getGridSequentially. ");
		// Add population of each block into the row/col of the grid.
		for (int i = 0; i < dataResults.getNumberOfDataPoints(); i++) {
			// round up the col/row numbers to break ties to the North/East.
			// row, col are indexed at 0.
			// LOGGER.log(Level.INFO, "QueryGrid: adding group at index i: " +
			// i);
			DataPoint currentDataPoint = dataResults.getDataPointAtIndex(i);
			addDataPointPopulationToGrid(currentDataPoint, this, grid_);
		}
		// LOGGER.log(Level.INFO, "MapUtilities: grid[1][1]: " + grid_[1][1]);
		// LOGGER.log(Level.INFO, "MapUtilities: grid[1][2]: " + grid_[1][2]);
		// LOGGER.log(Level.INFO, "MapUtilities: grid[2][1]: " + grid_[2][1]);
		return grid_;
	}
	
	// Takes in a grid where each grid cell holds an int representing the
	// population in that grid cell.
	// Returns a modified sum grid.
	private int[][] formatGridToModifiedSumSequentially() {
		// Update the grid according to the "modified sum" format:
		// "new grid[i][j]" = "original grid[i][j]" + grid[i-1][j] +
		// grid[i][j+1] - grid[i-1][j+1]
		// Indexed at 1, "summing" from top to bottom, left to right.
		for (int i = 1; i <= cols_; i++) { // cols
			for (int j = rows_; j > 0; j--) { // rows
				if (i > 1) // Only count the W block if not off the grid (i=0 is
							// not on the grid).
					grid_[i][j] += grid_[i - 1][j];
				if (j < rows_) // Only count the N block if not
												// off the grid (j=map.rows + 1
												// is not on the grid).
					grid_[i][j] += grid_[i][j + 1];
				if (i > 1 && j < rows_) // Only count the NW block
														// if not off the grid.
					grid_[i][j] -= grid_[i - 1][j + 1];
			}
		}
		return grid_;
	}
	
	// Uses the grid to get the Population.
	public int getPopulationInRectangle(GridRectangle rect) {
    	// Print some facts for the grid and query:
    	// Query dimensions.
    	LOGGER.log(Level.FINE, "Getting population in rectangle: " + rect);
    	// Grid dimensions.
    	LOGGER.log(Level.FINE, "Getting population in rectangle: grid (cols, rows): (" + Integer.valueOf(grid_.length  + 1) + ", " + Integer.valueOf(grid_[0].length - 1) + ")");
    	// Some sample reads of the grid's indices.
    	LOGGER.log(Level.FINE, "Getting population in rectangle: grid[1][1]: " + grid_[1][1]);
    	LOGGER.log(Level.FINE, "Getting population in rectangle: grid[1][2]: " + grid_[1][2]);
    	LOGGER.log(Level.FINE, "Getting population in rectangle: grid[2][1]: " + grid_[2][1]);
    	LOGGER.log(Level.FINE, "Getting population in rectangle: grid[2][2]: " + grid_[2][2]);
    	// Since: grid[right][bottom] + grid[right][top + 1] + grid[left - 1][bottom]
    	// - grid[left - 1][top + 1]
    	// Find each position, accounting for the grid which is indexed at 0, and query, 
    	// indexed at 1.
    	// Remember, grid stores columns first: grid[col][row]
    	
    	// Get census population
    	int bottomRight = grid_[rect.right_][rect.bottom_];
    	int bottomLeft = rect.left_ - 1 < 1 ? 0 : grid_[rect.left_ - 1][rect.bottom_]; 
    	int upperRight = rect.top_ == getRows() ? 0 :
    		grid_[rect.right_][rect.top_ + 1];
    	int upperLeft = rect.left_ < 1 || rect.top_ == getRows() ? 
    			0 : grid_[rect.left_ - 1][rect.top_ + 1];
    	LOGGER.log(Level.FINE, "bottomRight: " + bottomRight);
    	LOGGER.log(Level.FINE, "topRight: " + upperRight);
    	LOGGER.log(Level.FINE, "topleft: " + upperLeft);
    	LOGGER.log(Level.FINE, "bottomLeft: " + bottomLeft);
    	
    	// Represent the query as four corners on the grid to be calculated.
    	// The population relative to the query is: 
    	// grid[right][bottom] + grid[right][top + 1] + grid[left - 1][bottom] - grid[left - 1][top + 1]
    	// Want to avoid going off the map while finding the new grid coordinates.
    	// Also, account for translating the query (indexed at 1) to the grid (indexed at 0).
    	int popInRectangle = bottomRight - bottomLeft - upperRight + upperLeft;
    	return popInRectangle;
	}

	public static int getRowFromDataPoint(DataPoint dataPoint, QueryGrid queryGrid) {
		double ratio = Math.abs(dataPoint.latitude - queryGrid.minLat_) 
				/ Math.abs(queryGrid.maxLat_ - queryGrid.minLat_);
		int row = (int) Math.floor(ratio * queryGrid.rows_) + 1;
		if (row == queryGrid.getRows() + 1) // row is at the top of the grid
			// - usually row is equal to the
			// max latitude.
		{
			LOGGER.log(Level.WARNING, "edge case for row! row: " + row);
			return Integer.MAX_VALUE;
		}
		return row;
	}
	
	public static int getColFromGroup(DataPoint dataPoint, QueryGrid queryGrid) {
		double ratio = Math.abs((dataPoint.longitude - queryGrid.minLon_) 
				/ Math.abs(queryGrid.maxLon_ - queryGrid.minLon_));
		int col = (int) Math.floor(ratio * queryGrid.cols_) + 1;
		if (col == queryGrid.getCols() + 1) // col is at right of the grid -
			// usual col is equal to min
			// longitude.
		{
			LOGGER.log(Level.WARNING, "edge case for column! col: " + col);
			return Integer.MAX_VALUE;
		}
		return col;
	}

	// Given the census group and the grid, add the group to the 
	// proper row and column of the grid.
	// Used for fork-join framework.
    // A cell contains a group when the group is within [bottom, top)
    // and [left, right) of the cell.
	public static void addDataPointPopulationToGrid(DataPoint dataPoint, QueryGrid queryGrid, int[][] grid) {
		int row = getRowFromDataPoint(dataPoint, queryGrid);
		int col = getColFromGroup(dataPoint, queryGrid);
		// Handle case when rows/cols are on the North or East border.
		if (row == Integer.MAX_VALUE || col == Integer.MAX_VALUE)
			return;
		try {
			grid[col][row] += dataPoint.population; // Rows and cols are corrected for 1-based indexing
		} catch (ArrayIndexOutOfBoundsException e) {
			LOGGER.log(Level.FINE, "MapUtilities Error: total (row, col): ("
					+ queryGrid.getRows() + ", " + queryGrid.getCols() + ")");
			LOGGER.log(Level.FINE, "MapUtilities Error: current (row, col): ("
					+ row + ", " + col + ")");
			LOGGER.log(Level.FINE, "MapUtilities Error: current (lat, lon): ("
					+ dataPoint.latitude + ", " + dataPoint.longitude + ")");
			LOGGER.log(Level.FINE, "MapUtilities Error: (maxRealLat, maxLon): ("
					+ queryGrid.getMaxRealLat() + ", " + queryGrid.getMaxLon()
					+ ")");
			LOGGER.log(Level.FINE, "MapUtilities Error: (minRealLat, minLon): ("
					+ queryGrid.getMinRealLat() + ", " + queryGrid.getMinLon()
					+ ")");
			LOGGER.log(Level.FINE, "MapUtilities Error: data.data[i]: " + dataPoint);
			throw e;
		}
	}
	
    public int getRows() {
		return rows_;
	}

	public int getCols() {
		return cols_;
	}

	public double getMinLat() {
		return minLat_;
	}

	public double getMinRealLat() {
		return minRealLat_;
	}

	public double getMaxLat() {
		return maxLat_;
	}

	public double getMaxRealLat() {
		return maxRealLat_;
	}

	public double getMinLon() {
		return minLon_;
	}

	public double getMaxLon() {
		return maxLon_;
	}

	public double getRowHeight() {
		return rowHeight_;
	}

	public double getColLength() {
		return colLength_;
	}
    
    public int[][] getGrid() {
		return grid_;
	}

	public int[][] getModifiedSumGrid() {
		return modifiedSumGrid_;
	}

}