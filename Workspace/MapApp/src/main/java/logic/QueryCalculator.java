package main.java.logic;

import java.util.List;
import java.util.logging.Logger;

import main.java.utilities.Pair;

//public abstract class Versions {
public abstract class QueryCalculator {

    private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    
    public abstract QueryGrid getQueryGrid();
    public abstract DataResults getCensusResults();
    public abstract DataResults getTweetResults();

    public abstract List<Pair<Integer, Double>> processQuery(GridRectangle rect);
    
    public static Logger getLogger() {
    	return LOGGER;
    }
    
}
