package main.java.logic;


/*
 * This class wraps the latitude and longitude boundaries of our grid with the
 * rows and columns. 
 * Basically, this class serves as a tuple for the rows and columns that will be
 * in the grid.
 */
public class GridBoundaries {

    public int rows;
    public int cols;
//    // Dependent on MapPane! How else can we get the maximum boundaries for the
//    // map without depending on population data?
//    private float maxNorth = (float) MapPane.MAX_NORTH;
//    public float getMaxNorth() {
//		return maxNorth;
//	}
//
//	public float getMaxSouth() {
//		return maxSouth;
//	}
//
//	public float getMaxEast() {
//		return maxEast;
//	}
//
//	public float getMaxWest() {
//		return maxWest;
//	}
//
//	private float maxSouth = (float) MapPane.MAX_SOUTH;
//    private float maxEast = (float) MapPane.MAX_EAST;
//    private float maxWest = (float) MapPane.MAX_WEST;

    public GridBoundaries(int rows, int cols/*
                                         * , float maxNorth, float maxSouth,
                                         * float maxEast, float maxWest
                                         */) {
        this.rows = rows;
        this.cols = cols;
        // this.maxNorth = maxNorth;
        // this.maxEast = maxEast;
        // this.maxSouth = maxSouth;
        // this.maxWest = maxWest;
    }

//    public String toString() {
//        return "N: " + MapUtilities.calcGudermann(maxNorth) + "\tS: "
//                + MapUtilities.calcGudermann(maxSouth) + "\tE: " + maxEast
//                + "\tW: " + maxWest + "\trows: " + rows + "\tcols: " + cols;
//    }
}