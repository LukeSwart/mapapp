package main.java.logic;

import java.util.logging.Level;
import java.util.logging.Logger;

import main.java.utilities.Pair;

// Wrapper class containing all the results from evaluating the census data.
public class DataResults {

	private static final Logger LOGGER = Logger
			.getLogger(Logger.GLOBAL_LOGGER_NAME);
	
	private static final int INITIAL_SIZE = 100;
	private DataPoint[] data_;
	// Number of CensusGroups in the "data" field.
	private int data_size_;

	public double minLat;
	public double minRealLat;
	public double maxLat;
	public double maxRealLat;
	public double minLon;
	public double maxLon;
	
	public int total;

    public DataResults() {
    	data_ = new DataPoint[INITIAL_SIZE];
    	data_size_ = 0;

    	minLat = Double.MAX_VALUE;
        minRealLat = Double.MAX_VALUE;
        maxLat = (-1) * Double.MAX_VALUE;
        maxRealLat = (-1) * Double.MAX_VALUE;
        minLon = Double.MAX_VALUE;
        maxLon = (-1) * Double.MAX_VALUE;
        total = 0;
    }
    
    public DataPoint[] getDataPoints() {
    	return data_;
    }

    public DataPoint getDataPointAtIndex(int i) {
    	return data_[i];
    }

    public int getNumberOfDataPoints() {
    	return data_size_;
    }
    
//    public void addGroupAndUpdateMaxMin(CensusGroup group) {
//    	add(group);
//		updateMaxMinCoordinates(group);
//    }

	// Wrapper that creates a new census group from the given values.
    // Used for testing.
	public void addCensusGroup(int population, double latitude, double longitude) {
		add(new DataPoint(population, latitude, longitude));
	}
	
	// Adds a new census group to the array, resizing if necessary.
	public void add(DataPoint dataPoint) {
		if(data_size_ == data_.length) { // resize array as needed.
			DataPoint[] new_data = new DataPoint[data_.length*2];
			for(int i=0; i < data_.length; ++i)
				new_data[i] = data_[i];
			data_ = new_data;
		}
		data_[data_size_++] = dataPoint;
	}

	// Return a tuple representing the total population in the rectangle and the percent
	// of the total population represented by the population in that rectangle.
    public Pair<Integer, Double> getQueryResults(int popInRectangle) {
        double ratio = popInRectangle / (double) total;
        LOGGER.log(Level.FINE, "population ratio: " + ratio);
        String populationPercentage = String.format("%1$,.2f", ratio * 100);
        LOGGER.log(Level.FINE, "formatted population percentage: " + populationPercentage);
        Pair<Integer, Double> pr = new Pair<>(popInRectangle,
                Double.parseDouble(populationPercentage));
        return pr;
    }

    /* Updates the maximum and minimum coordinates, if needed, to the
     * coordinates of the CensusGroup.
     * Updates the population data as well.
     * Group must not be null.
     *
     * In the unlikely case that a census-block-group falls exactly
     * on the border of more than one grid position, tie-break by
     * assigning it to the North and/or East as needed.
     */
    public void updateMaxMinCoordinates(DataPoint dataPoint) {
        if (dataPoint.latitude > maxLat) {
            maxLat = dataPoint.latitude;
            maxRealLat = dataPoint.realLatitude;
        }
        if (dataPoint.latitude < minLat) {
            minLat = dataPoint.latitude;
            minRealLat = dataPoint.realLatitude;
        }
        if (dataPoint.longitude > maxLon)
            maxLon = dataPoint.longitude;
        if (dataPoint.longitude < minLon)
            minLon = dataPoint.longitude;
        total += dataPoint.population;
    }

    public String toString() {
        // return "minLat: " + MapUtilities.calcGudermann(minLat) + "\tmaxLat: "
        // + MapUtilities.calcGudermann(maxLat) + "\tminLon: " + minLon +
        // "\tmaxLon" + maxLon;
        return "minLat: " + minRealLat + "\tmaxLat: " + maxRealLat
                + "\tminLon: " + minLon + "\tmaxLon" + maxLon + "\tsize(groups): " + getNumberOfDataPoints();
    }
    
    @Override
    public boolean equals(Object other) {
    	if (!(other instanceof DataResults))
    		return false;
    	return equals((DataResults) other);
    }

    public boolean equals(DataResults other) {
    	if (getNumberOfDataPoints() != other.getNumberOfDataPoints() ||
    		maxLat != other.maxLat ||
    		Double.compare(minLat, other.minLat) != 0 ||
    		Double.compare(maxLat, other.maxLat) != 0 ||
    		Double.compare(maxRealLat, other.maxRealLat) != 0 ||
    		Double.compare(minRealLat, other.minRealLat) != 0 ||
    		Double.compare(total, other.total) != 0)
    		return false;
    	
    	for (int i = 0; i < getNumberOfDataPoints(); i++) {
    		DataPoint thisDataPoint = getDataPointAtIndex(i); 
    		DataPoint otherDataPoint = other.getDataPointAtIndex(i);
    		if (thisDataPoint.latitude != otherDataPoint.latitude ||
				thisDataPoint.longitude != otherDataPoint.longitude ||
				thisDataPoint.population != otherDataPoint.population)
    			return false;
    	}
    	return true;
    }

    // Merges the results from the parallel corner finding fork-join task.
    public DataResults merge(DataResults otherResults) {
		maxLat = Math.max(maxLat, otherResults.maxLat);
		minLat = Math.min(minLat, otherResults.minLat);
		maxRealLat = Math.max(maxRealLat, otherResults.maxRealLat);
		minRealLat = Math.min(minRealLat, otherResults.minRealLat);
		maxLon = Math.max(maxLon, otherResults.maxLon);
		minLon = Math.min(minLon, otherResults.minLon);
		total = total + otherResults.total;
		// Do NOT merge the groups - this is not part of the parallel fork-join task. 
//		for (Group group : otherResults.getdataGroups()) 
//			addGroupAndUpdateMaxMin(group);
		return this;
    }
//    
    @Override
    public int hashCode() {
    	int result = 17;
    	result = 31 * result + data_.hashCode();
    	result = 31 * result + data_size_;
    	result = 31 * result + total;
    	long minLatLong = Double.doubleToLongBits(minLat);
    	long maxLatLong = Double.doubleToLongBits(maxLat);
    	long minLonLong = Double.doubleToLongBits(minLon);
    	long maxLonLong = Double.doubleToLongBits(maxLon);
    	long minRealLatLong = Double.doubleToLongBits(minRealLat);
    	long maxRealLatLong = Double.doubleToLongBits(maxRealLat);
    	result = 31 * result + (int)(minLatLong ^ (minLatLong >>> 32));
    	result = 31 * result + (int)(maxLatLong ^ (maxLatLong >>> 32));
    	result = 31 * result + (int)(minLonLong ^ (minLonLong >>> 32));
    	result = 31 * result + (int)(maxLonLong ^ (maxLonLong >>> 32));
    	result = 31 * result + (int)(minRealLatLong ^ (maxRealLatLong >>> 32));
    	result = 31 * result + (int)(maxRealLatLong ^ (maxRealLatLong >>> 32));
    	return result;
    }
	public double getMinLat() {
		return minLat;
	}

	public double getMinRealLat() {
		return minRealLat;
	}

	public double getMaxLat() {
		return maxLat;
	}

	public double getMaxRealLat() {
		return maxRealLat;
	}

	public double getMinLon() {
		return minLon;
	}

	public double getMaxLon() {
		return maxLon;
	}
}