package main.java.logic;

import main.java.utilities.MapUtilities;

// A class to represent a rectangle shape with sides 
// measured in latitude and longitude coordinates.
public class CoordinateRectangle {
	
    // invariant: right >= left and top >= bottom (i.e., numbers get bigger as
    // you move up/right)
    // note in our census data longitude "West" is a negative number which
    // nicely matches bigger-to-the-right
    public double west_;
//    public double left;
    public double east_;
//    public double right;
    public double north_;
//    public double top;
    public double south_;
//    public double bottom;

    // Top and Bottom are in units of mercator latitude.
    public CoordinateRectangle(double west, double east, double north, double south) {
        west_ = west;
        east_ = east;
        north_ = north;
        south_ = south;
    }

    // a functional operation: returns a new Rectangle that is the smallest
    // rectangle containing this and that.
    public CoordinateRectangle encompass(CoordinateRectangle that) {
        return new CoordinateRectangle(Math.min(this.west_, that.west_), Math.max(
                this.east_, that.east_), Math.max(this.north_, that.north_),
                Math.min(this.south_, that.south_));
    }
    
    // Returns whether this rectangle contains the coordinates of the data point.
    // A rect contains a data point when the point is within [bottom, top)
    // and [left, right)
//    public boolean contains(CensusGroup group) {
    public boolean contains(DataPoint group) {
        boolean inRect = (group.latitude < north_ 
        			      && group.longitude < east_
    					  // group is north of or on the southern border of the rectangle.
    					  && (group.latitude >= south_) 
            			  // group is east of or on the western border of the rectangle.
            			  && (group.longitude >= west_)); 
        return inRect;
    }
    
    public String toString() {
        return "[left=" + west_ + " right=" + east_ + " top=" + north_ + " bottom="
                + south_ + "]";
    }

    // Returns the mercator values of the boundaries.
    public String toMapString() {
        return "[left=" + west_ + " right=" + east_ + " top="
//                + MapUtilities.calcGudermann(top) + " bottom="
                + MapUtilities.mercatorConversion(north_) + " bottom="
//                + MapUtilities.calcGudermann(bottom) + "]";
                + MapUtilities.mercatorConversion(south_) + "]";
    }
}