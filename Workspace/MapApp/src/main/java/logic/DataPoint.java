package main.java.logic;

import main.java.utilities.MapUtilities;

/*
 * A DataPoint represents a single line in the census population source
 * file. A census group is the finest granularity that we have of our data.
 */
public class DataPoint {
	public int population;
	public double realLatitude;
	public double latitude;
	public double longitude;
	
	public DataPoint(int pop, double lat, double lon) {
		population = pop;
		realLatitude = lat;
		latitude   = MapUtilities.mercatorConversion(lat);
		longitude  = lon;
	}
	
	public String toString() {
		return "(" + latitude + ", " + longitude + ") " + population;
	}
	
	public int getPopulation() {
		return population;
	}
}