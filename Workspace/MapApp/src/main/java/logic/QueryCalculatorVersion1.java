package main.java.logic;

import java.util.ArrayList;
import java.util.List;

import main.java.utilities.DataParser;
import main.java.utilities.MapUtilities;
import main.java.utilities.Pair;

/*
 * Before processing any queries, process the data to find the four corners of
 * the U.S. rectangle using a SEQUENTIAL O(n) algorithm where n is the number of
 * census-block-groups. 
 * Then for each query do another SEQUENTIAL O(n) traversal
 * to answer the query (determining for each census-block-group whether or not
 * it is in the query rectangle).
 */
public class QueryCalculatorVersion1 extends QueryCalculator {
	private final DataResults censusResults_;
	private final DataResults tweetResults_;
	private final QueryGrid queryGrid_;

//	private final GridBoundaries gridBoundaries;

	public QueryGrid getQueryGrid() {
		return queryGrid_;
	}

	@Override
	public DataResults getCensusResults() {
		return censusResults_;
	}

	@Override
	public DataResults getTweetResults() {
		return tweetResults_;
	}


	public QueryCalculatorVersion1(String censusFile, String tweetFile, int rows, int cols) {
		censusResults_ = DataParser.getCensusResultsSequentially(censusFile);
		tweetResults_ = DataParser.getTweetResultsSequentially(tweetFile);

		queryGrid_ = new QueryGrid(rows, cols, censusResults_);
	}

	// Assumes that the rectangle is given in grid coordinates.
	@Override
	public List<Pair <Integer, Double>> processQuery(GridRectangle gridRect) {
		CoordinateRectangle coordinateRect = MapUtilities
				.convertToCoordinateRect(gridRect, queryGrid_);
		// Check whether each block group is in the query rectangle.
		// If so, find the population in the rectangle.
		int popInRectangle = 0;
		for (int i = 0; i < censusResults_.getNumberOfDataPoints(); i++) {
			DataPoint group = censusResults_.getDataPointAtIndex(i);
			if (coordinateRect.contains(group))
				popInRectangle += group.population;
		}
		int tweetsInRectangle = 0;
		for (int i = 0; i < tweetResults_.getNumberOfDataPoints(); i++) {
			DataPoint tweet = tweetResults_.getDataPointAtIndex(i);
			if (coordinateRect.contains(tweet))
				tweetsInRectangle += tweet.population;
		}
		final List<Pair<Integer, Double>> queryResults = new ArrayList<>();
		Pair<Integer, Double> censusPair = censusResults_
				.getQueryResults(popInRectangle);
		Pair<Integer, Double> tweetPair = tweetResults_.getQueryResults(tweetsInRectangle);
		queryResults.add(censusPair);
		queryResults.add(tweetPair);
		return queryResults;
	}
}