package main.java.logic;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import main.java.utilities.MapAppLogger;
import main.java.utilities.Pair;
/*
 * Routes the user's interactions to the appropriate calls to process
 * the query.
 */
public class InteractionHandler {
    private static final boolean DEBUG = true;
    
    private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    
	public enum Version {
		ONE, TWO, THREE, FOUR, FIVE
	};
	public static Version running = Version.ONE;
	
	public static int getVersionNum() {
		switch (running) {
		case ONE:
			return 1;
		case TWO:
			return 2;
		case THREE:
			return 3;
		case FOUR:
			return 4;
		case FIVE:
			return 5;
		default:
			return -1;
		}
	}

    public static QueryCalculator populationCalculator;

    private static boolean isRunning = true;

    // Prompts the user to input data, returns the queried rectangle.
    // If the query does not fit the format, the program will exit.
    public static void query(PrintStream output, InputStream input) {
        // Get query rectangle from user.
        Scanner in = new Scanner(input);
        output.println("Enter rectangle query (W S E N): ");
        String query = in.nextLine();
        in.close();
        String[] splitQuery = query.split(" ");
        if (splitQuery .length != 4) {
            output.println("Exiting program.");
            exit();
            return;
        }
        List<Pair <Integer, Double>> queryResults = processQuery(splitQuery);
        if (queryResults == null)
            return;
        
        output.println("The total population in the queried rectangle: "
                + queryResults.get(0).getElementA());
        output.println("The percentage of the U.S. population in the queried rectangle, rounded to two decimal digits: "
                + queryResults.get(0).getElementB());
        output.println("The total tweets in the queried rectangle: "
        		+ queryResults.get(1).getElementA());
        output.println("The percentage of the U.S. tweets in the queried rectangle, rounded to two decimal digits: "
        		+ queryResults.get(1).getElementB());
    }
    
    // Assumes that the query is a valid String array containing the rows and columns of the rectangle's edges.
    public static List<Pair<Integer, Double>> processQuery(String[] splitQuery) {
        int west = Integer.parseInt(splitQuery[0]);
        int south = Integer.parseInt(splitQuery[1]);
        int east = Integer.parseInt(splitQuery[2]);
        int north = Integer.parseInt(splitQuery[3]);
        // Throw exception if invariants are broken.
        if (south > north || west > east)
            throw new IllegalArgumentException(
                    "north/east must be >= south/west, respectively.");
        return singleInteraction(west, south, east, north);
    }

    // Set the program to exit.
    private static void exit() {
        isRunning = false;
    }
    
    // Sets the static fields of this class based on the input data,
    // grid size (rows x cols) and the version number.
    public static QueryCalculator preprocess(String censusFileName, String tweetFileName, int cols, int rows,
            int version) {
        switch (version) {
        case 1:
        	running = Version.ONE;
            populationCalculator = new QueryCalculatorVersion1(censusFileName, tweetFileName, rows, cols);
            break;
        case 2:
        	running = Version.TWO;
            populationCalculator = new QueryCalculatorVersion2(censusFileName, tweetFileName, rows, cols);
            break;
        case 3:
        	running = Version.THREE;
        	populationCalculator = new QueryCalculatorVersion3(censusFileName, tweetFileName, rows, cols);
        	break;
        case 4:
        	running = Version.FOUR;
        	populationCalculator = new QueryCalculatorVersion4(censusFileName, tweetFileName, rows, cols);
        	break;
        case 5:
        	running = Version.FIVE;
        	populationCalculator = new QueryCalculatorVersion5(censusFileName, tweetFileName, rows, cols);
        	break;
        default:
        	LOGGER.log(Level.SEVERE, "Unrecognized version number.");
            break;
        }
        if (DEBUG) {
            DataResults censusResults = populationCalculator.getCensusResults();
            LOGGER.log(Level.INFO, "");
            LOGGER.log(Level.INFO, "VERSION CONSTRUCTED: " + version);
            LOGGER.log(Level.INFO, "Number of groups: " + censusResults.getNumberOfDataPoints() + "\tTotal population: " + censusResults.total);
            LOGGER.log(Level.INFO, "Cols: " + cols + "\tRows: " + rows);
            LOGGER.log(Level.INFO, "minRealLat: " + censusResults.minRealLat + "\tmaxRealLat: " + censusResults.maxRealLat
            		+ "\tminLong: " + censusResults.minLon + "\tmaxLong: " + censusResults.maxLon);
        }
        return populationCalculator;
    }

    // Provided with the sides of the query rectangle, returns a Pair with the
    // population and the population percentage inside the rectangle.
    // Assumes that the "w,s,e,n" coordinates are provided relative to the overlying grid.
    public static List<Pair<Integer, Double>> singleInteraction(int w, int s, int e,
            int n) {
        GridRectangle queryRect = new GridRectangle(w, e, n, s);
        if (DEBUG) {
        	LOGGER.log(Level.FINE, "");
        	LOGGER.log(Level.FINE, "GridRect passed to singleInteraction: " + queryRect);
        }
    	LOGGER.log(Level.FINE, "Processing simple query within rect: " + queryRect);
        List<Pair <Integer, Double>> queryResults = populationCalculator.processQuery(queryRect);

        if (DEBUG) {
            LOGGER.log(Level.FINE, "returning query results: " + Arrays.toString(queryResults.toArray()));
        }
        return queryResults;
    }

    /* The first three command-line arguments to the program are:
     * 1: file name for input data: pass this to parse
     * 2: number of x-dimension buckets
     * 3: number of y-dimension buckets
     * 4: number indicating the implementation version. The 5 versions are summarized below:
     */
    	/* Version 1: Simple and Sequential
    	 * Version 2: Simple and Parallel
    	 * Version 3: Smarter and Sequential
    	 * Version 4: Smarter and Parallel
    	 * Version 5: Smarter and Lock-Based
    	 */
    // Queries the user via console for their desired rectangle, and prints
    // the results (population and population percentage inside the rectangle.
    public static void main(String[] args) {
        try {
          MapAppLogger.setup();
        } catch (IOException e) {
          e.printStackTrace();
          throw new RuntimeException("Problems with creating the log files");
        }
        LOGGER.setLevel(Level.INFO);
        LOGGER.log(Level.INFO, "Logger initialized to global logger for query manager runtime.");

        String censusFileName = args[0];
        String tweetFileName = args[1];
        int cols = Integer.parseInt(args[2]);
        int rows = Integer.parseInt(args[3]);
        int version = Integer.parseInt(args[4]);

/* First process the data to find the four corners of the rectangle containing the United States.
 * Some versions of the program will then further preprocess the data to build a data structure 
 * that can efficiently answer the queries described above. The program will then prompt the user 
 * for such queries and answer them until the user chooses to quit. 
 */
        preprocess(censusFileName, tweetFileName, rows, cols, version);
        while (true) {
            query(System.out, System.in);
            if (!isRunning) {
                LOGGER.log(Level.INFO, "Exiting program.");
                return;
            }
        }
    }
}