package main.java.logic;

// A class to represent a Rectangle on the map.
// The bounds are measured in 
public class GridRectangle {
    // invariant: right >= left and top >= bottom (i.e., numbers get bigger as
    // you move up/right)
    // note in our census data longitude "West" is a negative number which
    // nicely matches bigger-to-the-right
    public int left_;
    public int right_;
    public int top_;
    public int bottom_;

    public GridRectangle(int l, int r, int t, int b) {
        left_ = l;
        right_ = r;
        top_ = t;
        bottom_ = b;
    }

//    // a functional operation: returns a new Rectangle that is the smallest
//    // rectangle containing this and that. 
//    public MercatorGridRectangle encompass(MercatorGridRectangle that) {
//        return new MercatorGridRectangle(Math.min(this.left, that.left),
//                Math.max(this.right, that.right), Math.max(this.top, that.top),
//                Math.min(this.bottom, that.bottom));
//    }
    
//    // Returns whether this rectangle contains the coordinates of the group.
//    // A rect contains a group when the group is within [bottom, top)
//    // and [left, right)
//    public boolean contains(CensusGroup group) {
//        boolean inRect = (group.latitude < top_ 
//        			      && group.longitude < right_
//    					  // group is north of or on the southern border of the rectangle.
//    					  && (group.latitude >= bottom_) 
//            			  // group is east of or on the western border of the rectangle.
//            			  && (group.longitude >= left_)); 
//        return inRect;
//    }


    public String toString() {
        return "[left=" + left_ + " right=" + right_ + " top=" + top_ + " bottom="
                + bottom_ + "]";
    }
}