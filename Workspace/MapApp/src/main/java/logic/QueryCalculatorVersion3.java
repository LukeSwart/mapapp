package main.java.logic;

import java.util.ArrayList;
import java.util.List;

import main.java.utilities.DataParser;
import main.java.utilities.Pair;

public class QueryCalculatorVersion3 extends QueryCalculator {
    private final DataResults censusResults_;
    private final DataResults tweetResults_;
    private final QueryGrid censusGrid_;
    private final QueryGrid tweetGrid_;

    public QueryCalculatorVersion3(String fileName, String tweetFileName, int rows, int cols) {
    	censusResults_ = DataParser.getCensusResultsSequentially(fileName);
    	tweetResults_ = DataParser.getTweetResultsSequentially(tweetFileName);
    	 
    	// Calculate the population grid.
    	censusGrid_ = new QueryGrid(rows, cols, censusResults_);
        censusGrid_.generateModifiedSumGrid_Sequentially(censusResults_);
        // Calculate the tweet grid.
        // Use the 2010 census data to define the boundaries of the grid.
        tweetGrid_ = new QueryGrid(rows, cols, censusResults_);
        tweetGrid_.generateModifiedSumGrid_Sequentially(tweetResults_);
        
    } 
    
    // Assumes that the rectangle is given in grid coordinates.
    // Assumes that the coordinates of the query rectangle are within the map boundaries.
    @Override
    public List<Pair<Integer, Double>> processQuery(GridRectangle rect) {
////    	// ex. 1st query could be "1 8 1 8" (W S E N), meaning 8 rows with 4 cols.
//    	int[][] populationGrid = queryGrid_.getGrid();
//    	// Print some facts for the grid and query:
//    	// Query dimensions.
//    	LOGGER.log(Level.FINE, "Version3: ProcessQuery: " + rect);
//    	// Grid dimensions.
//    	LOGGER.log(Level.FINE, "Version3: ProcessQuery: grid (cols, rows): (" + Integer.valueOf(populationGrid.length  + 1) + ", " + Integer.valueOf(populationGrid[0].length - 1) + ")");
//    	// Some sample reads of the grid's indices.
//    	LOGGER.log(Level.FINE, "Version3: ProcessQuery: grid[1][1]: " + populationGrid[1][1]);
//    	LOGGER.log(Level.FINE, "Version3: ProcessQuery: grid[1][2]: " + populationGrid[1][2]);
//    	LOGGER.log(Level.FINE, "Version3: ProcessQuery: grid[2][1]: " + populationGrid[2][1]);
//    	LOGGER.log(Level.FINE, "Version3: ProcessQuery: grid[2][2]: " + populationGrid[2][2]);
//    	// Since: grid[right][bottom] + grid[right][top + 1] + grid[left - 1][bottom]
//    	// - grid[left - 1][top + 1]
//    	// Find each position, accounting for the grid which is indexed at 0, and query, 
//    	// indexed at 1.
//    	// Remember, grid stores columns first: grid[col][row]
//    	
//    	// Get census population
//    	int bottomRight = populationGrid[rect.right_][rect.bottom_];
//    	int bottomLeft = rect.left_ - 1 < 1 ? 0 : populationGrid[rect.left_ - 1][rect.bottom_]; 
//    	int upperRight = rect.top_ == queryGrid_.getRows() ? 0 :
//    		populationGrid[rect.right_][rect.top_ + 1];
//    	int upperLeft = rect.left_ < 1 || rect.top_ == queryGrid_.getRows() ? 
//    			0 : populationGrid[rect.left_ - 1][rect.top_ + 1];
//    	LOGGER.log(Level.FINE, "bottomRight: " + bottomRight);
//    	LOGGER.log(Level.FINE, "topRight: " + upperRight);
//    	LOGGER.log(Level.FINE, "topleft: " + upperLeft);
//    	LOGGER.log(Level.FINE, "bottomLeft: " + bottomLeft);
//
//		// Get tweets
//    	int[][] tweetGrid = tweetGrid_.getGrid();
//    	int bottomRightTweets = populationGrid[rect.right_][rect.bottom_];
//    	int bottomLeftTweets = rect.left_ - 1 < 1 ? 0 : populationGrid[rect.left_ - 1][rect.bottom_]; 
//    	int upperRightTweets = rect.top_ == queryGrid_.getRows() ? 0 :
//    		populationGrid[rect.right_][rect.top_ + 1];
//    	int upperLeftTweets = rect.left_ < 1 || rect.top_ == queryGrid_.getRows() ? 
//    			0 : populationGrid[rect.left_ - 1][rect.top_ + 1];
//    	LOGGER.log(Level.FINE, "bottomRight: " + bottomRightTweets);
//    	LOGGER.log(Level.FINE, "topRight: " + upperRightTweets);
//    	LOGGER.log(Level.FINE, "topleft: " + upperLeftTweets);
//    	LOGGER.log(Level.FINE, "bottomLeft: " + bottomLeftTweets);
//    	
////    	// Represent the query as four corners on the grid to be calculated.
////    	// The population relative to the query is: 
////    	// grid[right][bottom] + grid[right][top + 1] + grid[left - 1][bottom] - grid[left - 1][top + 1]
////    	// Want to avoid going off the map while finding the new grid coordinates.
////    	// Also, account for translating the query (indexed at 1) to the grid (indexed at 0).
//    	int popInRectangle = bottomRight - bottomLeft - upperRight + upperLeft;
//    	int tweetsInRectangle = bottomRightTweets - bottomLeftTweets - upperRightTweets + upperLeftTweets;
    	
    	int popInRectangle = censusGrid_.getPopulationInRectangle(rect); 
    	int tweetsInRectangle = tweetGrid_.getPopulationInRectangle(rect); 
    	
		final List<Pair<Integer, Double>> queryResults = new ArrayList<>();
        Pair<Integer, Double> censusPair = censusResults_
                .getQueryResults(popInRectangle);
        Pair<Integer, Double> tweetPair = tweetResults_
        		.getQueryResults(tweetsInRectangle);
		queryResults.add(censusPair);
		queryResults.add(tweetPair);
		return queryResults;
    }

    @Override
    public DataResults getCensusResults() {  
        return censusResults_;
    }

	@Override
	public QueryGrid getQueryGrid() {
		return censusGrid_;
	}

	@Override
	public DataResults getTweetResults() {
		return tweetResults_;
	}
}