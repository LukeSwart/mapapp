package main.java.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import main.java.utilities.GenerateDataResultsInParallel;
import main.java.utilities.MapUtilities;
import main.java.utilities.Pair;
import main.java.utilities.SimpleDataPointPopulationQueryParallel;

public class QueryCalculatorVersion2 extends QueryCalculator {
//	private static final boolean DEBUG = true;

	private static final Logger LOGGER = Logger
			.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private DataResults censusResults_;
	private DataResults tweetResults_;
	private QueryGrid queryGrid_;

	
	@Override
	public QueryGrid getQueryGrid() {
		return queryGrid_;
	}

	@Override
	public DataResults getCensusResults() {
		return censusResults_;
	}
	
	@Override
	public DataResults getTweetResults() {
		return tweetResults_;
	}

	// Generates census results in parallel and 
	public QueryCalculatorVersion2(String censusFileName, String tweetFileName, int rows, int cols) {
		censusResults_ = GenerateDataResultsInParallel.getCensusResultsInParallel(censusFileName);
		tweetResults_ = GenerateDataResultsInParallel.getTweetResultsInParallel(tweetFileName);

		queryGrid_ = new QueryGrid(rows, cols, censusResults_);
		LOGGER.log(Level.INFO, "Created censusResults: " + censusResults_);
	}

	@Override
	public List<Pair<Integer, Double>> processQuery(GridRectangle gridRect) {
		CoordinateRectangle coordinateRect = MapUtilities
				.convertToCoordinateRect(gridRect, queryGrid_);
		LOGGER.log(Level.FINE, "Starting simple population parallel query...");
		int populationInRectangle = SimpleDataPointPopulationQueryParallel.find(
				censusResults_, coordinateRect);
		LOGGER.log(Level.INFO, "Population in rectangle: " + populationInRectangle);
		int tweetsInRectangle = SimpleDataPointPopulationQueryParallel.find(
				tweetResults_, coordinateRect);
		LOGGER.log(Level.INFO, "Tweets in rectangle: " + tweetsInRectangle);
		
//		final List<Pair<Integer, Double>> queryResults = (Pair<Integer, Double>[]) new Pair[2];
		final List<Pair<Integer, Double>> queryResults = new ArrayList<>();
		Pair<Integer, Double> censusPair = censusResults_.getQueryResults(populationInRectangle);
		Pair<Integer, Double> tweetPair = tweetResults_.getQueryResults(tweetsInRectangle);
		queryResults.add(censusPair);
		queryResults.add(tweetPair);
//		final DataResults[] 
//		queryResults[0] = censusPair;
//		queryResults[1] = tweetPair;
		return queryResults;
	}
}