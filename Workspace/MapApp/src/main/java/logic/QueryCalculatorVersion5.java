package main.java.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import main.java.utilities.DataParser;
import main.java.utilities.Pair;

public class QueryCalculatorVersion5 extends QueryCalculator {

	private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	
	private final DataResults censusResults_;
	private final DataResults tweetResults_;
	private final QueryGrid censusGrid_;
	private final QueryGrid tweetGrid_;
	// private final CensusData data;

	public QueryCalculatorVersion5(String fileName, String tweetFileName, int rows, int cols) {
		censusResults_ = DataParser.getCensusResultsSequentially(fileName);
		tweetResults_ = DataParser.getTweetResultsSequentially(tweetFileName);
		
		// Calculate the population grid.
		censusGrid_ = new QueryGrid(rows, cols, censusResults_);
		tweetGrid_ = new QueryGrid(rows, cols, censusResults_);
		int nthreads = 4;
		try {
			censusGrid_.generateModifiedSumGrid_LockBased(censusResults_, nthreads);
			tweetGrid_.generateModifiedSumGrid_LockBased(tweetResults_, nthreads);
		} catch (InterruptedException e) {
			LOGGER.log(Level.SEVERE, "Interruption for locked-based grid generation: ", e);
		}
		
	}

	// Same as version 3... (the modified sum grid precessing is all that differs).
	@Override
	public List<Pair<Integer, Double>> processQuery(GridRectangle rect) {
////    	// ex. 1st query could be "1 8 1 8" (W S E N), meaning 8 rows with 4 cols.
////    	GridRectangle queryRect = MapUtilities.convertToGridRect(coordinateRect, 
////    															censusResults, mapResults);
//		int[][] grid = queryGrid_.getGrid();
//    	// Print some facts for the grid and query:
//    	// Query dimensions.
//    	LOGGER.log(Level.FINE, "Version5: ProcessQuery: " + rect);
//    	// Grid dimensions.
//    	LOGGER.log(Level.FINE, "Version5: ProcessQuery: grid (cols, rows): (" + Integer.valueOf(grid.length  + 1) + ", " + Integer.valueOf(grid[0].length - 1) + ")");
//    	// Some sample reads of the grid's indices.
////    	LOGGER.log(Level.FINE, "Version5: ProcessQuery: grid[1][1]: " + grid[1][1]);
////    	LOGGER.log(Level.FINE, "Version5: ProcessQuery: grid[1][2]: " + grid[1][2]);
////    	LOGGER.log(Level.FINE, "Version5: ProcessQuery: grid[2][1]: " + grid[2][1]);
////    	LOGGER.log(Level.FINE, "Version5: ProcessQuery: grid[2][2]: " + grid[2][2]);
//    	// Since: grid[right][bottom] + grid[right][top + 1] + grid[left - 1][bottom]
//    	// - grid[left - 1][top + 1]
//    	// Find each position, accounting for the grid which is indexed at 0, and query, 
//    	// indexed at 1.
//    	// grid[col][row] !!!
//    	int bottomRight = grid[rect.right_][rect.bottom_];
//    	int bottomLeft = rect.left_ - 1 < 1 ? 0 : grid[rect.left_ - 1][rect.bottom_]; 
//    	int upperRight = rect.top_ == queryGrid_.getRows() ? 0 :
//    		grid[rect.right_][rect.top_ + 1];
//    	int upperLeft = rect.left_ < 1 || rect.top_ == queryGrid_.getRows() ? 
//    			0 : grid[rect.left_ - 1][rect.top_ + 1];
//    	LOGGER.log(Level.FINE, "bottomRight: " + bottomRight);
//    	LOGGER.log(Level.FINE, "topRight: " + upperRight);
//    	LOGGER.log(Level.FINE, "topleft: " + upperLeft);
//    	LOGGER.log(Level.FINE, "bottomLeft: " + bottomLeft);
//    	
////    	// Represent the query as four corners on the grid to be calculated.
////    	// The population relative to the query is: 
////    	// grid[right][bottom] + grid[right][top + 1] + grid[left - 1][bottom] - grid[left - 1][top + 1]
////    	// Want to avoid going off the map while finding the new grid coordinates.
////    	// Also, account for translating the query (indexed at 1) to the grid (indexed at 0).
////    	GridRectangle gridRect = new GridRectangle(Math.max(1, queryRect.left), queryRect.right + 1, Math.min(mapResults.cols, queryRect.top + 2), queryRect.bottom + 1);
//////    	GridRectangle gridRect = queryRect.encompass(encompassingRect);
////    	int popInRectangle = grid[gridRect.right][gridRect.bottom] + grid[gridRect.left][gridRect.bottom] + grid[gridRect.right][gridRect.top] - grid[gridRect.left][gridRect.top];
////    	int popInRectangle = bottomRight - bottomLeft - upperRight + upperLeft;

    	int popInRectangle = censusGrid_.getPopulationInRectangle(rect);
    	int tweetsInRectangle = tweetGrid_.getPopulationInRectangle(rect);
    	
        
		final List<Pair<Integer, Double>> queryResults = new ArrayList<>();
        Pair<Integer, Double> censusPair = censusResults_
                .getQueryResults(popInRectangle);
        Pair<Integer, Double> tweetPair = tweetResults_
        		.getQueryResults(tweetsInRectangle);
		queryResults.add(censusPair);
		queryResults.add(tweetPair);
		return queryResults;

	}

	@Override
	public DataResults getCensusResults() {
		return censusResults_;
	}

	@Override
	public DataResults getTweetResults() {
		return tweetResults_;
	}

	@Override
	public QueryGrid getQueryGrid() {
		return censusGrid_;
	}
}