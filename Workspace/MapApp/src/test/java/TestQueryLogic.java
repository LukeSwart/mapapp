package test.java;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import main.java.logic.DataPoint;
import main.java.logic.DataResults;
import main.java.logic.InteractionHandler;
import main.java.logic.QueryCalculator;
import main.java.logic.QueryGrid;
import main.java.utilities.DataParser;
import main.java.utilities.GenerateDataResultsInParallel;
import main.java.utilities.MapAppLogger;
import main.java.utilities.MapUtilities;
import main.java.utilities.Pair;

import org.junit.Before;
import org.junit.Test;

public class TestQueryLogic {
	private static final boolean DEBUG = false;
	private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private static final String TEST_POPULATION_FILE = "TestPopulations.txt";
	private static final String TEST_CENSUS_RESULTS = "TestCensusResults.txt";
	private static final String TEST_QUERY_FILE = "TestQueries.txt";
	private static final String FULL_CENSUS_FILE = "CenPop2010.txt";
	private static final String FULL_TWEET_FILE = "USTweets.txt";

	private static final int ROWS = 8;
	private static final int COLS = 4;
	private static final int MIN_VERSION = 5;
	private static final int MAX_VERSION = 5;
	private static final int FULL_POPULATION = 312471327;

	private static final double EPSILON = 0.00001;
	private static final double EPSILON2 = 0.02;
	double minLat = Integer.MAX_VALUE;
	double maxLat = Integer.MIN_VALUE;
	double minLon = Integer.MAX_VALUE;
	double maxLon = Integer.MIN_VALUE;
	int total_;
	DataResults censusResults_;

	private static boolean setUpIsDone = false;

	@Before
	// Initializes the test and testing files.
	public void setUp() {
		if (!setUpIsDone) {
			// Initialize the logger
			try {
				MapAppLogger.setup();
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException(
						"Problems with creating the log files");
			} finally {
				setUpIsDone = true;
			}
		}
		LOGGER.info("\n");
		LOGGER.info("Global logger is initialized for testing.");

		LOGGER.info("working directory: "
				+ Paths.get("").toAbsolutePath());
		censusResults_ = DataParser.parseCensusData(FULL_CENSUS_FILE);
		// Find the four corners of the census results.
		for (int i = 0; i < censusResults_.getNumberOfDataPoints(); i++) {
			DataPoint dataPoint = censusResults_.getDataPointAtIndex(i);
			censusResults_.updateMaxMinCoordinates(dataPoint);
		}
		LOGGER.info("Creating test files.");
		createTestFiles(TEST_POPULATION_FILE, TEST_QUERY_FILE);
	}

	// private CensusResults generateTestCensusResults() {
	// LOGGER.info("");
	// LOGGER.info("TestQueries: Generating test census results.");
	// CensusResults censusResults =
	// CensusResultsCalculator.getResultsSequentially(TEST_CENSUS_RESULTS);

	// Manual census results upload
	// CensusResults censusResults = new CensusResults();
	// // Create a grid (0 to 80 latitude x -100 to 0 longitude)
	// // indices are: (population, latitude, longitude)
	// censusResults.add(0, 0, 0); // SE corner
	// censusResults.add(0, 80, -100); // NW corner
	// censusResults.add(1, 0, -100); // SW corner
	// censusResults.add(0, 80, 0); // NE corner
	// censusResults.add(1, 9, -76);
	// censusResults.add(22, 79, -76);
	// censusResults.add(19, 69, -76);
	// censusResults.add(4, 19, -76);
	// censusResults.add(7, 29, -76);
	// censusResults.add(2, 9, -51);
	// censusResults.add(3, 9, -26);
	// censusResults.add(8, 29, -51);
	// censusResults.add(16, 59, -76);
	//
	// // Add each group to our Census results.
	// for (int i = 0; i < censusResults.getNumberOfGroups(); i++) {
	// CensusGroup group = censusResults.getGroupAtIndex(i);
	// censusResults.updateMaxMinCoordinates(group);
	// }
	// return censusResults;
	// }

	 @Test
	// Prepares a PopulationQuery using a test population file. Reads queries
	// from another test file and checks the results against pre-determined
	// values
	// located within the test files.
	public void testVersionsWithTestFile() {
		LOGGER.info("\n");
		LOGGER.info("TestQueries: testVersionsWithTestFile BEGINS.");
		for (int versionNumber = MIN_VERSION; versionNumber <= MAX_VERSION; versionNumber++) {
			InteractionHandler.preprocess(TEST_POPULATION_FILE, FULL_TWEET_FILE, COLS, ROWS,
					versionNumber); // Prepare the population query by reading
									// in the test file.
			try {
				BufferedReader reader = new BufferedReader(new FileReader(
						TEST_QUERY_FILE));
				String nextLine;// ordinals are: "W S E N"
				while ((nextLine = reader.readLine()) != null) {
					LOGGER.info("\n");
					List<Pair <Integer, Double>> queryResults = InteractionHandler
							.processQuery(nextLine.split(" "));
					int expected = Integer.parseInt(reader.readLine());
					if (DEBUG)
						LOGGER.log(Level.INFO, 
								"TestQueries: testVersionsWithTestFile: processing queries: (expected: "
										+ expected + ", actual: "
										+ queryResults.get(0).getElementA() + ")");
//					assertEquals(expected, (int) pr.getElementA());
				}
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			// Hard-coded grid results when debugging the test.
//			 assertEquals(grid[0][0], 2);
//			 assertEquals(grid[1][0], 2);
//			 assertEquals(grid[0][1], 11);
//			 assertEquals(grid[0][7], 22);
//			 assertEquals(grid[0][6], 0);
			LOGGER.log(Level.INFO, 
					"TestQueries: testVersionsWithTestFile ENDS.");
		}

		// MapResults mr = v.getMapResults();
		// Generate random coordinate Rectangles.
		// Random r = new Random();
		// for (int i = 0; i < NUM_SAMPLES; i++) {
		// double w = (r.nextdouble() * (mr.maxWest - mr.maxLon) + v.maxLon); //
		// Longitudes always negative
		// double e = (r.nextdouble() * (w - v.maxLon)) + v.maxLon;
		// double n = (r.nextdouble() * (v.maxLat - v.minLat) + v.minLat);
		// double s = r.nextdouble() * (n - v.minLat) + v.minLat;
		// LOGGER.info();
		// LOGGER.info("n: " + n + "\ts: " + s + "\te: " + e +
		// "\tw: " + w);
		// assertTrue(n >= s);
		// assertTrue(e >= w);
		// Rectangle rect = new Rectangle(w, e, n, s);
		// LOGGER.info(rect);
		// Pair<Integer, double> pr = v.processQuery(rect);
		// LOGGER.info("TestQueries: pr: " + pr);
		// }
	}

	@Test
	// This applies to version 3 and above for our grid data structure.
	// Check our test grid to ensure that it has the values we think that it
	// should have.
	// In the unlikely case that a census-block-group falls exactly
	// on the border of more than one grid position, tie-break by
	// assigning it to the North and/or East as needed.
	public void testGridSequentially() {
		DataResults censusResults = DataParser
				.getCensusResultsSequentially(TEST_CENSUS_RESULTS);
		// Create our grid from
		int[][] grid = new QueryGrid(ROWS, COLS, censusResults).generateGrid_Sequentially(censusResults);
		// assertEquals(results.maxLat, MapUtilities.calcGudermann(80),
		// EPSILON);
		// assertEquals(results.minLat, MapUtilities.calcGudermann(0), EPSILON);

		LOGGER.info("");
		// Check mercator latitude results for our latitude boundaries - used
		// for plugging in test values.
		// LOGGER.info(MapUtilities.mercatorConversion(9));//
		// 0.15772961
		// LOGGER.info(MapUtilities.mercatorConversion(19));//
		// 0.3378629
		// LOGGER.info(MapUtilities.mercatorConversion(29));//
		// 0.52925265
		// LOGGER.info(MapUtilities.mercatorConversion(39));//
		// 0.7402901
		// LOGGER.info(MapUtilities.mercatorConversion(49));//
		// 0.98380786
		// LOGGER.info(MapUtilities.mercatorConversion(59));//
		// 1.2825668
		// LOGGER.info(MapUtilities.mercatorConversion(69));//
		// 1.6855683
		// LOGGER.info(MapUtilities.mercatorConversion(79));//
		// 2.340401
		// LOGGER.info(MapUtilities.mercatorConversion(80));//
		// 2.436246
		// LOGGER.info(MapUtilities.mercatorConversion(9));// 0.157

		double x = MapUtilities.mercatorConversion(80);// 2.436246
		// Print the grid boundaries.
		for (int i = 1; i < ROWS; i++) {
			double xStep = x * i / ROWS;
			LOGGER.log(Level.INFO, 
					"Mercator latitude: " + xStep + "\tDegrees latitude: "
							+ MapUtilities.calcGudermann(xStep) + "\tRows: "
							+ i);
		}

		// Grid boundaries.
		// Mercator latitude Latitude in degrees Row number
		// 0.30453074 17.184725 1
		// 0.6090615 32.919907 2
		// 0.9135922 46.290447 3
		// 1.218123 57.045162 4
		// 1.5226537 65.38939 5
		// 1.8271844 71.72272 6
		// 2.1317153 76.468834 7

		// Longitude in degrees Col number
		// -100 1
		// -75 2
		// -50 3
		// -25 4

		LOGGER.info("");
		// Test the grid with some known values.
		assertEquals(censusResults.maxRealLat, 80, EPSILON2);
		assertEquals(censusResults.minRealLat, 0, EPSILON2);
		assertEquals(censusResults.minLon, -100, EPSILON2);
		assertEquals(censusResults.maxLon, 0, EPSILON2);
		// NOTE: rows/cols indexed at 1 here: Row, Col
		assertEquals(grid[1][1], 2); // 1 1
		assertEquals(grid[2][1], 2); // 1 2
		assertEquals(grid[1][2], 11); // 2 1
		assertEquals(grid[1][8], 22); // 8 1
		assertEquals(grid[1][7], 0); // 7 1

		LOGGER.info("TestQueries: End testGridSequentially.");
	}

	// Using the TEST DATA file, tests the grid that is created (Versions 3 and up) 
	// for equality with the grid that is generated sequentially (versions 1 and 2).
	// The grids should be equal.
	@Test
	public void compareCensusResultsCalculation_SequentialVsParallel_testFile() {
		LOGGER.info("\n");
		LOGGER.log(Level.INFO, 
				"comparing parallel vs sequential census results on test file BEGINS...");
		DataResults sequentialResults = DataParser
				.getCensusResultsSequentially(TEST_CENSUS_RESULTS);
		LOGGER.log(Level.INFO, 
				"created sequential results: " + sequentialResults);
		DataResults parallelResults = GenerateDataResultsInParallel
				.getCensusResultsInParallel(TEST_CENSUS_RESULTS);
		LOGGER.log(Level.INFO, 
				"created parallel results: " + parallelResults);
		assertEquals(sequentialResults, parallelResults);
		LOGGER.log(Level.INFO, 
				"comparing parallel vs sequential census results ENDS...");
	}
	
	// Using the 2010 CENSUS DATA file, tests the grid that is created (Versions 3 and up) 
	// for equality with the grid that is generated sequentially (versions 1 and 2).
	// The grids should be equal.
	@Test
	public void compareCensusResultsCalculation_SequentialVsParallel_fullFile() {
		LOGGER.info("\n");
		LOGGER.log(Level.INFO, 
				"comparing parallel vs sequential census results on 2010 census file BEGINS...");
		DataResults sequentialResults = DataParser
				.getCensusResultsSequentially(FULL_CENSUS_FILE);
		LOGGER.log(Level.INFO, 
				"created sequential results: " + sequentialResults);
		DataResults parallelResults = GenerateDataResultsInParallel
				.getCensusResultsInParallel(FULL_CENSUS_FILE);
		LOGGER.log(Level.INFO, 
				"created parallel results: " + parallelResults);
		assertEquals(sequentialResults, parallelResults);
		LOGGER.log(Level.INFO, 
				"comparing parallel vs sequential census results ENDS...");
	}

	@Test
	public void compareModifiedSumGrids_SequentialVsParallel_testFile() {
		LOGGER.log(Level.INFO, 
				"compareModifiedSumGrids: comparing sequential vs parallel using test file BEGINS.");
		DataResults censusResults = DataParser
				.getCensusResultsSequentially(TEST_CENSUS_RESULTS);
		int[][] sequentialGrid = new QueryGrid(ROWS, COLS, censusResults).generateModifiedSumGrid_Sequentially(censusResults);
		LOGGER.log(Level.INFO, 
				"compareSumGrids: Generated ModifiedSumGrid sequentially.");
		int[][] parallelGrid = new QueryGrid(ROWS, COLS, censusResults).generateModifiedSumGrid_Parallel(censusResults);
		LOGGER.log(Level.INFO, 
				"compareSumGrids: Generated ModifiedSumGrid in parallel.");
		int rows = sequentialGrid.length;
		int cols = sequentialGrid[0].length;
		assertEquals(rows, parallelGrid.length);
		assertEquals(cols, parallelGrid[0].length);
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				assertEquals(sequentialGrid[i][j], parallelGrid[i][j]);
			}
		}
	}

	// Using the full 2010 census data, tests the grid that is created (Versions 3 and up) 
	// for equality with the grid that is generated sequentially (versions 1 and 2).
	// The grids should be equal.
	@Test
	public void compareModifiedSumGrids_SequentialVsParallel_fullFile() {
		// Get the census results (parallel or sequentially doesn't matter
		// because it is not being tested.
		LOGGER.log(Level.INFO, 
				"compareModifiedSumGrids sequential vs parallel testing BEGINS...");
		DataResults censusResults = DataParser
				.getCensusResultsSequentially(FULL_CENSUS_FILE);
		
		QueryGrid sequentialQueryGrid = new QueryGrid(ROWS, COLS, censusResults);
		int[][] sequentialModifiedSumGrid = sequentialQueryGrid.generateModifiedSumGrid_Sequentially(censusResults);
		LOGGER.log(Level.INFO, 
				"compareModifiedSumGrids: Generated ModifiedSumGrid sequentially.");
		QueryGrid parallelQueryGrid = new QueryGrid(ROWS, COLS, censusResults);
		int[][] parallelModifiedSumGrid = parallelQueryGrid.generateModifiedSumGrid_Parallel(censusResults);
		LOGGER.log(Level.INFO, 
				"compareMOdifieSumGrids: Generated ModifiedSumGrid in parallel.");
		int rows = sequentialModifiedSumGrid[0].length;
		int cols = sequentialModifiedSumGrid.length;
		assertEquals(cols, parallelModifiedSumGrid.length);
		assertEquals(rows, parallelModifiedSumGrid[0].length);
		assertEquals(sequentialQueryGrid.getRowHeight(), parallelQueryGrid.getRowHeight(), EPSILON);
		assertEquals(sequentialQueryGrid.getColLength(), parallelQueryGrid.getColLength(), EPSILON);
		assertEquals(rows, parallelModifiedSumGrid[0].length);
		for (int i = 0; i < cols; i++) {
			for (int j = 0; j < rows; j++) {
				LOGGER.log(Level.INFO, 
						"comparing col: " + i + ", row: " + j 
						+ "\nsequentialGrid: " + sequentialModifiedSumGrid[i][j]
								+ "\nparallelGrid: " + parallelModifiedSumGrid[i][j]);
				assertEquals(sequentialModifiedSumGrid[i][j], parallelModifiedSumGrid[i][j]);
			}
		}
	}

	// Using the full 2010 census data, tests the grid that is created (Versions 3 and up) 
	// for equality with the grid that is generated sequentially (versions 1 and 2).
	// The grids should be equal.
	@Test
	public void compareElementGrids_SequentialVsParallel_fullFile() {
		// Get the census results (parallel or sequentially doesn't matter
		// because it is not being tested.
		LOGGER.log(Level.INFO, 
				"compareElementGrids sequential vs parallel testing BEGINS...");
		DataResults censusResults = DataParser
				.getCensusResultsSequentially(FULL_CENSUS_FILE);
		
		QueryGrid sequentialQueryGrid = new QueryGrid(ROWS, COLS, censusResults);
		int[][] sequentialElementGrid = sequentialQueryGrid.generateElementGrid_Sequentially(censusResults);
		LOGGER.log(Level.INFO, 
				"Generated element grid sequentially.");
		QueryGrid parallelQueryGrid = new QueryGrid(ROWS, COLS, censusResults);
		int[][] parallelElementGrid = parallelQueryGrid.generateElementGrid_Parallel(censusResults);
		LOGGER.log(Level.INFO, 
				"Generated element grid in parallel.");
		int cols = sequentialElementGrid.length;
		int rows = sequentialElementGrid[0].length;
		assertEquals(cols, parallelElementGrid.length);
		assertEquals(rows, parallelElementGrid[0].length);
		assertEquals(sequentialQueryGrid.getRowHeight(), parallelQueryGrid.getRowHeight(), EPSILON);
		assertEquals(sequentialQueryGrid.getColLength(), parallelQueryGrid.getColLength(), EPSILON);
		assertEquals(rows, parallelElementGrid[0].length);
		for (int i = 0; i < cols; i++) {
			for (int j = 0; j < rows; j++) {
				LOGGER.log(Level.INFO, 
						"comparing col: " + i + ", row: " + j 
						+ "\nsequentialGrid: " + sequentialElementGrid[i][j]
								+ "\nparallelGrid: " + parallelElementGrid[i][j]);
				assertEquals(sequentialElementGrid[i][j], parallelElementGrid[i][j]);
			}
		}
	}
	// Using the full 2010 census data, tests the lock-based grid that is created (Version 5) 
	// for equality with the grid that is generated sequentially (versions 1 and 2).
	// The grids should be equal.
	@Test
	public void compareElementGrids_ParallelVsLockBased_fullFile() {
		// Get the census results (parallel or sequentially doesn't matter
		// because it is not being tested.
		LOGGER.log(Level.INFO, 
				"compareElementGrids parallel vs lockBased testing BEGINS...");
		DataResults censusResults = DataParser
				.getCensusResultsSequentially(FULL_CENSUS_FILE);
		
		QueryGrid lockBasedQueryGrid = new QueryGrid(ROWS, COLS, censusResults);
		int nthreads = 4;
		int[][] lockBasedElementGrid = null;
		try {
			lockBasedElementGrid = lockBasedQueryGrid.generateElementGrid_LockBased(censusResults, nthreads);
		} catch (InterruptedException e) {
			LOGGER.log(Level.SEVERE, "Exception during lock based grid generation: " + e);
		}
		if (lockBasedElementGrid == null)
		{
			assertTrue(false);
			return;
		}
		LOGGER.log(Level.INFO, 
				"Generated element grid lock-based.");
		QueryGrid parallelQueryGrid = new QueryGrid(ROWS, COLS, censusResults);
		int[][] parallelElementGrid = parallelQueryGrid.generateElementGrid_Parallel(censusResults);
		LOGGER.log(Level.INFO, 
				"Generated element grid in parallel.");
		int cols = lockBasedElementGrid.length;
		int rows = lockBasedElementGrid[0].length;
		assertEquals(cols, parallelElementGrid.length);
		assertEquals(rows, parallelElementGrid[0].length);
		assertEquals(lockBasedQueryGrid.getRowHeight(), parallelQueryGrid.getRowHeight(), EPSILON);
		assertEquals(lockBasedQueryGrid.getColLength(), parallelQueryGrid.getColLength(), EPSILON);
		assertEquals(rows, parallelElementGrid[0].length);
		LOGGER.info(
				"lock-based elemental grid:\n" + Arrays.toString(lockBasedElementGrid));
		LOGGER.info( 
				"parallel elemental grid:\n" + Arrays.toString(parallelElementGrid));
		boolean areEqual = true;
		for (int i = 0; i < cols; i++) {
			for (int j = 0; j < rows; j++) {
				LOGGER.info(
						"comparing col: " + i + ", row: " + j 
						+ "\nlockBasedGrid: " + lockBasedElementGrid[i][j]
								+ "\nparallelGrid: " + parallelElementGrid[i][j]);
				if (DEBUG) { // Continue execution throughout grid if we are debugging.
					if (lockBasedElementGrid[i][j] != parallelElementGrid[i][j])
						areEqual = false;
				} else {
					assertEquals(lockBasedElementGrid[i][j], parallelElementGrid[i][j]);
				}
			}
		}
		if (DEBUG)
			assertTrue(areEqual);
	}
	
	 @Test
	// Manually tests all versions using hard-coded queries.  
	public void testVersionsWithFullFile() {
		LOGGER.info("\n");
		LOGGER.info("TestQueries: testVersionsWithFullFile BEGINS.");
		boolean areEqual = true;
		for (int versionNumber = MIN_VERSION; versionNumber <= MAX_VERSION; versionNumber++) {
			QueryCalculator calculator = InteractionHandler.preprocess(
					FULL_CENSUS_FILE, FULL_TWEET_FILE, COLS, ROWS, versionNumber);
			assertEquals(FULL_POPULATION, calculator.getCensusResults().total);
			// W S E N
			String[] queries = { "1 8 1 8", "2 3 3 4", "1 7 1 7", "1 5 1 6",
					"3 1 4 2" };
			int[] results = {7855, 52239808, 118418, 495940, 129173427};
			for (int i = 0; i < queries.length; i++) {
				LOGGER.info("\n");
				List<Pair <Integer, Double>> queryResults = InteractionHandler.processQuery(queries[i]
						.split(" "));
				int expected = results[i];
				LOGGER.info("Version " + versionNumber + 
						" Population in rectangle: expected: " + expected + 
						", actual: " + queryResults.get(0).getElementA());
				if (DEBUG) { // Continue execution throughout all grids and versions if we are debugging.
					if (queryResults.get(0).getElementA() != expected)
						areEqual = false;
				} else {
					assertEquals(expected, (int) queryResults.get(0).getElementA());
				}
			}
		}
		LOGGER.info("TestQueries: testVersionsWithFullFile ENDS.");
		if (DEBUG)
			assertTrue(areEqual);
	}

	@Test
	// Manually tests sequential vs. grid style queries by comparing only the
	// single cell within a grid to the single rectangle in a grid.
	// Helps determine whether error is coming from modified sum equation or
	// from the single-celled grid itself.
	// To enter a single cell query, must have a one-by-one query.
	public void singleElementConsistencyTest() {
		LOGGER.info("\n");
		LOGGER.info("TestQueries: testVersionsWithFullFile BEGINS.");
		boolean areEqual = true;
		int sequentialVersion = 1;
		int gridVersion = 3;
		// assertEquals(FULL_POPULATION,
		// sequentialCalculator.getCensusResults().total);
		// assertEquals(FULL_POPULATION,
		// gridCalculator.getCensusResults().total);
		// W S E N  **OR** Col Row Col Row because W == E and S == N
		String[] queries = { "1 8 1 8", "2 4 2 4", "1 7 1 7", "1 5 1 5",
				"1 6 1 6", "4 8 4 8", "4 4 4 4", "4 1 4 1", "3 2 3 2", 
				"2 1 2 1", "1 1 1 1"};
		// int[] results = { 7855, 52064198, 118418, 495940, 129379589 };
		// int[] results = {10653, 52239808, 118418, 495940, 129173427}; //
		// Diverging results...
		for (int i = 0; i < queries.length; i++) {
			// Generate a population via sequential processing.
			InteractionHandler.preprocess(FULL_CENSUS_FILE, FULL_TWEET_FILE, COLS, ROWS, sequentialVersion);
			String[] query = (queries[i].split(" "));
			List<Pair <Integer, Double>> queryResults = InteractionHandler.processQuery(query);
			int sequentialExpected = queryResults.get(0).getElementA();
			// Generate a population via grid processing on the same query.
			// Assumes that the version is 3 and up.
			QueryCalculator gridCalculator = InteractionHandler.preprocess(
					FULL_CENSUS_FILE, FULL_TWEET_FILE, COLS, ROWS, gridVersion);
			DataResults censusResults = gridCalculator.getCensusResults();
			QueryGrid queryGrid = new QueryGrid(ROWS, COLS, censusResults);
			int[][] singleCellGrid = queryGrid.generateElementGrid_Sequentially(censusResults);
			int col = Integer.parseInt(query[0]); // "East/West" - 1 cell width
			int row= Integer.parseInt(query[1]); // "North/South" - 1 cell height
			int gridExpected = singleCellGrid[col][row]; // 1 based index for the grid.
			LOGGER.info("\n");
			// int expected = results[i];
			LOGGER.info("Single cell query processed for sequential, grid: \n"
					+ sequentialExpected + ", " + gridExpected
					+ "\nAt row, col: \n" + row + ", " + col);
			if (DEBUG) { // Continue execution throughout all grids and versions
							// if we are debugging.
				if (sequentialExpected != gridExpected)
					areEqual = false;
			} else {
				assertEquals(sequentialExpected, gridExpected);
			}
		}
		LOGGER.info("TestQueries: testVersionsWithFullFile ENDS.");
		if (DEBUG)
			assertTrue(areEqual);
	}

	@Test
	public void testGudermann() {
		LOGGER.info("");
		LOGGER.info("TestQueries: testGudermann BEGINS.");
		double maxSouth = (double) 17.941346;
		double maxSouthMerc = MapUtilities.mercatorConversion(maxSouth);
		double maxSouthGudermann = MapUtilities.calcGudermann(maxSouthMerc);
		assertEquals(maxSouth, maxSouthGudermann, EPSILON);

		double maxNorth = (double) 71.300949;
		double maxNorthMerc = MapUtilities.mercatorConversion(maxNorth);
		double maxNorthGudermann = MapUtilities.calcGudermann(maxNorthMerc);
		assertEquals(maxNorth, maxNorthGudermann, EPSILON);

		double maxRealLat = 80;
		double minRealLat = 0;
		double topRow1 = MapUtilities.getRowTopFromRealLatitude(10, 1, ROWS,
				minRealLat, maxRealLat);
		assertEquals(topRow1, 17.184725, EPSILON);
		double topRow2 = MapUtilities.getRowTopFromRealLatitude(10, 2, ROWS,
				minRealLat, maxRealLat);
		assertEquals(topRow2, 32.919907, EPSILON);
		LOGGER.info("TestQueries: testGudermann ENDS.");
	}

	// Map must be a mercator projection.
	private static void createTestFiles(String populationFileName,
			String queryFileName) {
		try {
			PrintWriter populationFileOut = new PrintWriter(new BufferedWriter(
					new FileWriter(populationFileName)));
			LOGGER.info("Printwriter created.");
			/*
			 * Use format like below to fill out file:
			 * STATEFP,COUNTYFP,TRACTCE,BLKGRPCE,POPULATION,LATITUDE,LONGITUDE
			 * 01,001,020100,1,0,100,-100 01,001,020100,2,0,0,-100
			 */
			double maxRealLat = 80;
			double minRealLat = 0;
			double realLatDelta = 10;

			double minLon = -100;
			double maxLon = -0;
			double lonDelta = 25;
			int population = 1;
			populationFileOut
					.println("STATEFP,COUNTYFP,TRACTCE,BLKGRPCE,POPULATION,LATITUDE,LONGITUDE");
			for (double lat = minRealLat + realLatDelta - 1; lat < maxRealLat; lat += realLatDelta) {
				for (double lon = minLon + lonDelta - 1; lon < maxLon
						- lonDelta; lon += lonDelta) {
					populationFileOut.println("0,0,0,0," + population++ + ","
							+ lat + "," + lon);
				}
			}
			// print corners.
			populationFileOut.println("0,0,0,0,1," + maxRealLat + "," + maxLon);
			populationFileOut.println("0,0,0,0,1," + minRealLat + "," + maxLon);
			populationFileOut.println("0,0,0,0,1," + maxRealLat + "," + minLon);
			populationFileOut.println("0,0,0,0,1," + minRealLat + "," + minLon);
			populationFileOut.close();

			PrintWriter queryFileOut = new PrintWriter(new BufferedWriter(
					new FileWriter(queryFileName)));

			// topRow3 = 46.29
			// topRow4 = 57.05

			// Format is : W S E N real latitude span
			queryFileOut.println("1 1 1 1");// 0-17.18
			queryFileOut.println("2");
			queryFileOut.println("2 1 2 1");
			queryFileOut.println("2");
			queryFileOut.println("1 1 2 2");// 0 - 32.92
			queryFileOut.println("28");
			queryFileOut.println("1 4 2 4"); // 46.3 - 57
			queryFileOut.println("27");
			queryFileOut.println("2 5 3 8");// 57 - 80
			queryFileOut.println("123");
			queryFileOut.println("2 4 3 8");// 46 - 80
			queryFileOut.println("152");
			queryFileOut.close();
		} catch (IOException e) {
			System.out
					.println("TestQueries: Creating test files: Error while writing file.");
			e.printStackTrace();
		}
	}
}